import numpy
import cv2
from glob2 import glob
import scipy.ndimage.filters

images = glob('samples/*.jpg')
#print(images)
img = cv2.imread(images[0], 0)

filter = scipy.ndimage.filters.sobel(img)
#filter = cv2.Laplacian(img, cv2.CV_16S)
#filter = cv2.Canny(img,10,50)

cv2.imwrite('sample_procesed/filtered_' + images[0][8:], numpy.float32(filter))
