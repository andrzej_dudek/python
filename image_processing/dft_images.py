import cv2
import numpy

# fourier transform
img = cv2.imread('samples/Lighthouse.jpg', 0)
f = numpy.fft.fft2(img)
fshift = numpy.fft.fftshift(f)
magnitude_spectrum = 20*numpy.log(numpy.abs(fshift))
cv2.imwrite('sample_procesed/mag_spec_Lighthouse.jpg', magnitude_spectrum)

#highpass filtering and coming back to the time spectrum
rows, cols = img.shape
crow, ccols = int(rows/2), int(cols/2)
fshift[crow-30:crow+30, ccols-30:ccols+30] = 0
f_ishift = numpy.fft.ifftshift(fshift)
img_back = numpy.fft.ifft2(f_ishift)
img_back = numpy.abs(img_back)
cv2.imwrite('sample_procesed/img_back_Lighthouse.jpg', img_back)

# the same but using opencv functions because they are faster
dft = cv2.dft(numpy.float32(img), flags = cv2.DFT_COMPLEX_OUTPUT)
dft_shift = numpy.fft.fftshift(dft)
magnitude_spectrum = 20*numpy.log(cv2.magnitude(dft_shift[:,:,0],dft_shift[:,:,1]))
cv2.imwrite('sample_procesed/mag_spec_cv_Lighthouse.jpg', magnitude_spectrum)

rows, cols = img.shape
crow, ccols = int(rows/2), int(cols/2)
dft_shift[crow-30:crow+30, ccols-30:ccols+30] = 0
f_ifftshift = numpy.fft.ifftshift(dft_shift)
img_back = cv2.idft(numpy.float32(f_ifftshift), flags=cv2.DFT_REAL_OUTPUT | cv2.DFT_SCALE)
#img_back = cv2.magnitude(img_back[:,:,0], img_back[:,:,1])
cv2.imwrite('sample_procesed/img_back_cv_Lighthouse.jpg', img_back)


#optimalize version of fft
rows, cols = img.shape
nrows = cv2.getOptimalDFTSize(rows)
ncols = cv2.getOptimalDFTSize(cols)
nimg = numpy.zeros((nrows,ncols))
nimg[:rows,:cols] = img
dft = cv2.dft(numpy.float32(nimg), flags=cv2.DFT_COMPLEX_OUTPUT)
dft_shift = numpy.fft.fftshift(dft)
magnitude_spectrum = 20*numpy.log(cv2.magnitude(dft_shift[:,:,0],dft_shift[:,:,1]))
rows, cols = nimg.shape
crow, ccols = int(rows/2), int(cols/2)
dft_shift[crow-30:crow+30, ccols-30:ccols+30] = 0
f_ifftshift = numpy.fft.ifftshift(dft_shift)
img_back = cv2.idft(numpy.float32(f_ifftshift), flags=cv2.DFT_REAL_OUTPUT | cv2.DFT_SCALE)
cv2.imwrite('sample_procesed/img_hpf_Lighthouse.jpg', img_back)
