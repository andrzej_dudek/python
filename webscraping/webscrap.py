import requests, pandas
from bs4 import BeautifulSoup

dict_list = []
base_url = "http://www.pyclass.com/real-estate/rock-springs-wy/LCWYROCKSPRINGS/t=0&s="

r = requests.get(base_url + "0.html",
                headers={'User-agent':'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:61.0) Gecko/20100101 Firefox/61.0'})

content = r.content
soup = BeautifulSoup(content, "html.parser")
page_number = int(soup.find_all("a",{"class":"Page"})[-1].text) * 10

for page in range(0,page_number,10):
    r = requests.get(base_url + str(page) + ".html",
                    headers={'User-agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:61.0) Gecko/20100101 Firefox/61.0'})
    content = r.content
    soup = BeautifulSoup(content, "html.parser")
    all = soup.find_all("div",{"class":"propertyRow"})

    for item in all:
        dic = {}
        try:
            dic["Price"] = item.find_all("h4", {"class":"propPrice"})[0].text.strip()
        except:
            dic["Price"] = None
        try:
            dic["Street"] = item.find_all("span", {"class":"propAddressCollapse"})[0].text
        except:
            dic["Street"] = None
        try:
            dic["Town"] = item.find_all("span", {"class":"propAddressCollapse"})[1].text
        except:
            dic["Town"] = None
        try:
            dic["Beds"] = item.find("span", {"class":"infoBed"}).find("b").text
        except:
            dic["Beds"] = None
        try:
            dic["Full Baths"] = item.find("span", {"class":"infoValueFullBath"}).find("b").text
        except:
            dic["Full Baths"] = None
        try:
            dic["Square Feet"] = item.find("span", {"class":"infoSqFt"}).find("b").text
        except:
            dic["Square Feet"] = None
        try:
            dic["Half Baths"] = item.find("span", {"class":"infoValueHalfBath"}).find("b").text
        except:
            dic["Half Baths"] = None


        for column_group in item.find_all("div",{"class":"columnGroup"}):
            for feature_group, feature_name in zip(column_group.find_all("span",{"class":"featureGroup"}), column_group.find_all("span",{"class":"featureName"})):
                if "Lot size" in feature_group.text:
                    dic["Lot Size"] = feature_name.text

        if "Lot Size" not in dic.keys():
            dic["Lot Size"] = None

        dict_list.append(dic)

df = pandas.DataFrame(dict_list)
df.to_csv("Output.csv")
