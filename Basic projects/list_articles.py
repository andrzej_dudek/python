#importujemy potrzebne biblioteki
#requests do łączenia ze stronami www
#i funkcję BeautifulSoup do obsługi html
import requests
from bs4 import BeautifulSoup as b_soup

#funkcja do wyświetlania wszystkich tytułów artykułów ze strony www.onet.pl
def list_articles():

    #podajemy url i pobieramy go za pomocą funkcji get()
    url = 'http://www.onet.pl'
    r = requests.get(url)

    #zamieniamy nasz text w tekst html
    soup = b_soup(r.text, "html5lib")

    #znajdujemy wszystkie tytuły za pomocą funkcji find_all
    for story_heading in soup.find_all("span", class_="title"):
        
        if story_heading.a :
            print(story_heading.a.replace("\n", " ").strip())
        else:
            print(story_heading.contents[0].strip())

list_articles()
