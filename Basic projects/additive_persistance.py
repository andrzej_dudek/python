def additive_persistance(number):
    #this function counts an additive
    #persistance of a number
    adding_res = 0

    if int(number) <= 9:
        return 0

    for n in number:
        adding_res += int(n)

    additive_persistance_count = 1

    while adding_res >= 10:
        helping_num = str(adding_res)
        adding_res = 0

        for n in helping_num:
            adding_res += int(n)

        additive_persistance_count += 1

    return additive_persistance_count

while True:
    number = input("Number: ")
    print("Additive persistance of the number is:", additive_persistance(number))
