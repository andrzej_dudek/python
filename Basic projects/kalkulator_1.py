def dodawanie(dzialanie):
    x=[]
    for i in dzialanie:
        if i == ' ':
            continue
        elif i != '+':
            x.append(int(i))
        elif i == '+':
            length_x = len(x)
            x_ = 0
            for k in x:
                x_ = x_ + (k * (10 ** (length_x - 1)))
                length_x = length_x - 1
            x = []

    y_ = 0
    length_x = len(x)

    for l in x:
        y_ = y_ + (l * (10 ** (length_x - 1)))
        length_x = length_x - 1

    x = []
    result = x_ + y_
    print("Ans =", result)

#End function

def odejmowanie(dzialanie):
    x=[]
    for i in dzialanie:
        if i == ' ':
            continue
        elif i != '-':
            x.append(int(i))
        elif i == '-':
            length_x = len(x)
            x_ = 0
            for k in x:
                x_ = x_ + (k * (10 ** (length_x - 1)))
                length_x = length_x - 1
            x = []

    y_ = 0
    length_x = len(x)

    for l in x:
        y_ = y_ + (l * (10 ** (length_x - 1)))
        length_x = length_x - 1

    x = []
    result = x_ - y_
    print("Ans =", result)

#End function

def mnozenie(dzialanie):
    x=[]
    for i in dzialanie:
        if i == ' ':
            continue
        elif i != '*':
            x.append(int(i))
        elif i == '*':
            length_x = len(x)
            x_ = 0
            for k in x:
                x_ = x_ + (k * (10 ** (length_x - 1)))
                length_x = length_x - 1
            x = []

    y_ = 0
    length_x = len(x)

    for l in x:
        y_ = y_ + (l * (10 ** (length_x - 1)))
        length_x = length_x - 1

    x = []
    result = x_ * y_
    print("Ans =", result)

#End function

def dzielenie(dzialanie):
    x=[]
    for i in dzialanie:
        if i == ' ':
            continue
        elif i != '/':
            x.append(int(i))
        elif i == '/':
            length_x = len(x)
            x_ = 0
            for k in x:
                x_ = x_ + (k * (10 ** (length_x - 1)))
                length_x = length_x - 1
            x = []

    y_ = 0
    length_x = len(x)

    for l in x:
        y_ = y_ + (l * (10 ** (length_x - 1)))
        length_x = length_x - 1

    x = []
    try:
        result = x_ / y_
        print("Ans =", result)
    except ZeroDivisionError:
        print("Nie można dzielić przez zero!")

print("=== Podstawowy kalkulator w Pythonie ===")
print("=== Aby zakończyć wpisz 'quit' ===")
print("=== Aby wykonać obliczenia wpisz działanie ===")

while True:

    user_input = input()

    try:
        if "+" in user_input:
            dodawanie(user_input)
        elif "-" in user_input:
            odejmowanie(user_input)
        elif "*" in user_input:
            mnozenie(user_input)
        elif "/" in user_input:
            dzielenie(user_input)
        elif "quit" in user_input:
            print("Program kończy pracę.")
            break
        else:
            print("Kalkulator nie obsługuje takiego działania!")
            print("Wprowadź dane na nowo")
    except:
        print("Kalkulator nie obsługuje takiego działania!")
        print("Wprowadź dane na nowo")
        continue

#End of the while loop
