import random

def generate_random_text(N):
    i = 0
    random_text = ""
    letters = "abcdefghijklmnopqrstuvwxyz"

    while i < N:
        random_text = random_text + random.choice(letters)
        i += 1

    return random_text

###

def count_char(text, char):
    counter = 0

    for c in text:
        if c == char:
            counter += 1

    return counter

###

num = int(input("Ile liter chcesz mieć w pliku: "))
text = generate_random_text(num)

try:
    file = open("losowytekst.txt", "w")
    file.write(text)
finally:
    file.close()

try:
    file = open("losowytekst.txt", "r")
    text_to_analyze = file.read()
finally:
    file.close()

try:
    file = open("analizalosowego.txt", "w")
    file.write("Analiza częstości występowania danej litry:")
finally:
    file.close()

for i in "abcdefghijklmnopqrstuvwxyz":
    prec = 0
    prec = 100 * count_char(text_to_analyze, i) / len(text_to_analyze)
    line = "\n{char} - {per}%".format(char = i, per = round(prec,2))
    try:
        file = open("analizalosowego.txt", "a")
        file.write(line)
    finally:
        file.close()
