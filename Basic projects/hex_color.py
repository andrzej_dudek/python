def hex_rgb_convert(number):
    if number < 1:
        number = number * 255
    number = number / 16
    num_int = int(number)
    num_float = number - num_int

    hex_alphabet = "0123456789ABCDEF"
    first = hex_alphabet[num_int]
    num_float = num_float * 255 / 16
    num_float = round(num_float)
    second = hex_alphabet[num_float]
    color = first + second
    return color


def hex_color(red, green, blue):
    color = "#"
    color += hex_rgb_convert(red)
    color += hex_rgb_convert(green)
    color += hex_rgb_convert(blue)
    print(color)

while True:
    red = float(input("red:"))
    green = float(input("green:"))
    blue = float(input("blue:"))
    hex_color(red, green, blue)
