def subfactorial(n):
    # function that will count sufactorials of an n-element objects
    if (int(n) - n) != 0:
        print("Invalid arguments")

    if n == 1:
        return 0
    if n == 0:
        return 1

    sub = (n - 1) * (subfactorial(n - 1) + subfactorial(n - 2))
    return sub

while True:
    n = int(input("n = "))
    print(subfactorial(n))
