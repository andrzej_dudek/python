def upc(code):              # as string

    if len(code) != 11:
        print("Invalid code")
        return

    odd_sum = 0
    even_sum = 0

    for i in range(len(code)):
        if i % 2 == 0:
            odd_sum += int(code[i])
        else:
            even_sum += int(code[i])

    M = ((3 * odd_sum) + even_sum) % 10

    if M == 0:
        res = 0
    else:
        res = 10 - M

    print("M=", res)
    return res

while True:
    user_input = input("UPC digits: ")
    upc(user_input)
