import random

def remove_spaces(word):
    return_word = ""
    for n in range(len(word)):
        if word[n] == " ":
            continue
        else:
            return_word += word[n]
    return return_word

def dice_roll(word):
    #dice rolling function
    word = remove_spaces(word)
    d_position = word.find("d")
    rolls = 0

    for i in range(len(word)):
        if word[i] == "d":
            break
        else:
            rolls += int(word[i]) * (10 ** (d_position - i - 1))

    d_range = 0
    d_range_length = len(word) - d_position

    for i in range(d_range_length - 1):
        if (d_position + i + 1) == len(word):
            break
        else:
            d_range += int(word[d_position + i + 1]) * (10 ** (d_range_length - i - 2))

    result = 0

    for i in range(rolls):
        result += random.randint(1, d_range)

    return result


while True:
    roll = input("Roll: ")
    print(dice_roll(roll))
