import random

try:
    file = open("pliklosowy.txt", "w")
    file.write("Teraz nastąpi generacja liczb losowych od 1 do 100")
finally:
    file.close()

for i in range(20):
    try:
        file = open("pliklosowy.txt", "a")
        x = str(random.randint(1,101))
        file.write("\n")
        file.write(x)
    finally:
        file.close()

try:
    file = open("pliklosowy.txt", "r")
    file_content = file.read()
    print(file_content)
finally:
    file.close()
