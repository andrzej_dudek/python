def add_one_to_every_digit(digit):
    #this function add one to every digit in the input
    new_number = []
    digit = str(digit)

    for n in digit:
        num = int(n) + 1
        new_number.append(num)

    num_to_return = ""

    for n in new_number:
        num_to_return += str(n)

    return num_to_return

while True:
    number = input("Type a number:")
    print("After adding one to every digit the output is:")
    print(add_one_to_every_digit(number))
