def remove_spaces(word):
    return_word = ""
    for n in range(len(word)):
        if word[n] == " ":
            continue
        else:
            return_word += word[n]
    return return_word

def funel(f_word, s_word):
    #this function determine if the second word can be made out of first word
    #by removing only one letter
    f_word = remove_spaces(f_word)
    s_word = remove_spaces(s_word)
    k = 0
    for n in range(len(f_word)):
        comp_word = ""

        for l in range(len(f_word)):
            if l == k:
                continue
            comp_word += f_word[l]

        k = k + 1
        if comp_word == s_word:
            return True
        if k == len(f_word):
            return False
        else:
            continue

while True:
    first = input("First word: ")
    second = input("Second word: ")
    print(funel(first, second))
