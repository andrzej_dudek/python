from pandas_datareader import data
from bokeh.plotting import figure, show, output_file
from datetime import datetime

def plot(name):
    end = datetime.now()

    def get_start():
        now = datetime.now()
        if now.month < 4:
            start = datetime(now.year-1, 12 - (now.month%3), 1)
        else:
            start = datetime(now.year, now.month-3, 1)
        return start

    start = get_start()

    df = data.DataReader(name=name, data_source="yahoo", start=start, end=end)

    def increase_decrease(op,cl):
        if op > cl:
            return "Decrease"
        elif op < cl:
            return "Increase"
        else:
            return "Equal"

    df["Status"] = [increase_decrease(op,cl) for op,cl in zip(df.Open, df.Close)]
    df["Middle"] = (df.Open + df.Close)/2
    df["Abs"] = abs(df.Open - df.Close)

    f = figure(x_axis_type="datetime",width=1000,height=300)
    f.title.text = "\"" + name + "\" stock chart"
    f.grid.grid_line_alpha = 0.3
    f.sizing_mode = "scale_both"

    hours_12 = 12 * 60 * 60 * 1000

    f.segment(df.index,df.High,df.index,df.Low,color="#000000")

    f.rect(df.index[df.Status=="Increase"],
           df.Middle[df.Status=="Increase"],
           hours_12,
           df.Abs[df.Status=="Increase"],
           fill_color="#9FF3A7",
           line_color="#000000")

    f.rect(df.index[df.Status=="Decrease"],
           df.Middle[df.Status=="Decrease"],
           hours_12,
           df.Abs[df.Status=="Decrease"],
           fill_color="#D57686",
           line_color="#000000")

    f.rect(df.index[df.Status=="Equal"],
           df.Middle[df.Status=="Equal"],
           hours_12,
           df.Abs[df.Status=="Equal"],
           fill_color="#D57686",
           line_color="#000000")
    return f

f1 = plot("GOOG")
f2 = plot("AAPL")
f3 = plot("AMZN")

output_file("plot.html")
show(f1)
show(f2)
show(f3)
