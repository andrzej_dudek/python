import cv2
import time

#video = cv2.VideoCapture("file_name")
video = cv2.VideoCapture(0,)

a = 1

while True:
    check, frame = video.read()

    #print(check)
    #print(frame)

    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    gray = cv2.resize(gray, (int(gray.shape[1]/2), int(gray.shape[0]/2)))
    cv2.imshow("Capturing", gray)
    a += 1

    cv2.waitKey(int(1000/120))
    key = cv2.waitKey(1)

    if key == ord('q'):
        break

    #print(gray.shape)
    #print(frame.shape)

video.release()
print(a)
cv2.destroyAllWindows()
