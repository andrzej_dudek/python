To run this script you need a haarcascade file,
you can find some in:

https://github.com/opencv/opencv/tree/master/data/haarcascades

or you can try to find some other source for 
your haarcascades but remember that they must be 
compatible with opencv library
