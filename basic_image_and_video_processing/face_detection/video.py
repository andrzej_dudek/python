import cv2

face_cascade = cv2.CascadeClassifier("haarcascade_frontalface_default.xml")

video = cv2.VideoCapture(0)

while True:
    check, frame = video.read()
    #frame = cv2.resize(frame, (int(frame.shape[1]/2), int(frame.shape[0]/2)))

    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    faces = face_cascade.detectMultiScale(gray,
    scaleFactor=1.1,
    minNeighbors=7)

    for x,y,w,h in faces:
        frame = cv2.rectangle(frame, (x,y), (x+w, y+h), (255,0,255))

    cv2.imshow("Capturing", frame)
    key = cv2.waitKey(1000)
    cv2.waitKey(int(1000/240))

    if key == ord('q'):
        break

video.release()
cv2.destroyAllWindows()
