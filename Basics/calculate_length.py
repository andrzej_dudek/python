def calculate_length(input_string):
    if type(input_string) == int:
        return "Sorry, integers don't have length"
    elif type(input_string) == float:
        return "Sorry, floats don't have length"
    else:
        return len(input_string)

user_input = input("Enter your input: ")
print(calculate_length(user_input))
print(calculate_length(10))
