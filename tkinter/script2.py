from tkinter import *

window = Tk()

e1_value = StringVar()
e1 = Entry(window, textvariable=e1_value)
e1.grid(row=0, column=1)

def convert():
    kg = float(e1_value.get())
    t1.delete(1.0, END)
    t1.insert(END, str(kg * 1000))
    t2.delete(1.0, END)
    t2.insert(END, str(kg * 2.20462))
    t3.delete(1.0,END)
    t3.insert(END, str(kg * 35.274))

t1 = Text(window, height=1, width=25)
t1.grid(row=2, column=0)

t2 = Text(window, height=1, width=25)
t2.grid(row=2, column=1)

t3 = Text(window, height=1, width=25)
t3.grid(row=2, column=2)

l0 = Label(window, text="Kilograms (kg)")
l0.grid(row=0,column=0)

l1 = Label(window, text="Grams (g)")
l1.grid(row=1, column=0)

l2 = Label(window, text="Pounds (lbs)")
l2.grid(row=1, column=1)

l3 = Label(window, text="Ounces (oz)")
l3.grid(row=1, column=2)

b1 = Button(window, text="Convert", command=convert)
b1.grid(row=0, column=2)

window.mainloop()
