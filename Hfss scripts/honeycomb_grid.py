# ----------------------------------------------
# Script Recorded by ANSYS Electronics Desktop Version 2021.1.0
# 9:25:41  kwi 19, 2024
# ----------------------------------------------
import ScriptEnv
import math

def calculateDistance(x, y):
    return math.sqrt(x ** 2 + y** 2)


def calculatePermittivity(distance):
    if distance < 1:
        return 11.9
    permittivity = 1.803e-8 * distance**3 - 2.931e-5 * distance**2 + 5.309e-4*distance + 11.89
    return permittivity

def calculateSlotRadius(permittivity):
    if permittivity == 11.9:
        return 0
    radius = -0.2971 * permittivity**3 + 6.353 * permittivity**2 - 52.32*permittivity + 227.4
    return radius


ScriptEnv.Initialize("Ansoft.ElectronicsDesktop")
oDesktop.RestoreWindow()
oProject = oDesktop.SetActiveProject("Project1")
oDesign = oProject.SetActiveDesign("HFSSDesign1")
oEditor = oDesign.SetActiveEditor("3D Modeler")
oEditor.Copy(
	[
		"NAME:Selections",
		"Selections:="		, "Polygon1"
	])

Smax = 7
Sbound = int(Smax/2)
iter = 1
hexcell_radius = 100 / math.cos(math.pi/6)

for n in range(-Sbound, Sbound+1):
    cell_number = Smax - abs(n)
    Cbound = int(cell_number/2)
    y = str(n) + "*3/2*hexcell_radius"
    ynum = n * 3/2 * hexcell_radius
    for k in range(-Cbound, Cbound+1):
        if cell_number % 2 == 0 and k == -Cbound:
            pass
        else:
            iter = iter + 1
            if cell_number % 2 == 0: 
                x = "sqrt(3)*hexcell_radius*(" + str(k) + "-0.5)"
                xnum = math.sqrt(3) * hexcell_radius * (k-0.5)
            else:
                x = "sqrt(3)*hexcell_radius*" + str(k)
                xnum = math.sqrt(3) * hexcell_radius * k
            oEditor.Paste()
            oEditor.Move(
                [
                    "NAME:Selections",
                    "Selections:="		, "Polygon"+str(iter),
                    "NewPartsModelFlag:="	, "Model"
                ], 
                [
                    "NAME:TranslateParameters",
                    "TranslateVectorX:="	, x,
                    "TranslateVectorY:="	, y,
                    "TranslateVectorZ:="	, "0mm"
                ])
            distance = calculateDistance(xnum, ynum)
            permittivity = calculatePermittivity(distance)
            radius = calculateSlotRadius(permittivity)
            if radius == 0:
                pass
            else:
                oEditor.CreateCylinder(
                [
                    "NAME:CylinderParameters",
                    "XCenter:="		, x,
                    "YCenter:="		, y,
                    "ZCenter:="		, "-2*SiThick",
                    "Radius:="		, str(radius)+"um",
                    "Height:="		, "SiThick",
                    "WhichAxis:="		, "Z",
                    "NumSides:="		, "0"
                ], 
                [
                    "NAME:Attributes",
                    "Name:="		, "Cylinder"+str(iter-1),
                    "Flags:="		, "",
                    "Color:="		, "(143 175 143)",
                    "Transparency:="	, 0,
                    "PartCoordinateSystem:=", "Global",
                    "UDMId:="		, "",
                    "MaterialValue:="	, "\"vacuum\"",
                    "SurfaceMaterialValue:=", "\"\"",
                    "SolveInside:="		, True,
                    "ShellElement:="	, False,
                    "ShellElementThickness:=", "0mm",
                    "IsMaterialEditable:="	, True,
                    "UseMaterialAppearance:=", False,
                    "IsLightweight:="	, False
                ])
    
    """
    if cell_number % 2 == 1:
        for k in range(-Cbound, Cbound+1):
            iter = iter + 1
            x = "sqrt(3)*hexcell_radius*" + str(k)
            oEditor.Paste()
            oEditor.Move(
                [
                    "NAME:Selections",
                    "Selections:="		, "Polygon"+str(iter),
                    "NewPartsModelFlag:="	, "Model"
                ], 
                [
                    "NAME:TranslateParameters",
                    "TranslateVectorX:="	, x,
                    "TranslateVectorY:="	, y,
                    "TranslateVectorZ:="	, "0mm"
                ])

    else:    
        for k in range(-Cbound+1, Cbound+1):
            iter = iter + 1
            x = "sqrt(3)*hexcell_radius*(" + str(k) + "-0.5)"
            oEditor.Paste()
            oEditor.Move(
                [
                    "NAME:Selections",
                    "Selections:="		, "Polygon"+str(iter),
                    "NewPartsModelFlag:="	, "Model"
                ], 
                [
                    "NAME:TranslateParameters",
                    "TranslateVectorX:="	, x,
                    "TranslateVectorY:="	, y,
                    "TranslateVectorZ:="	, "0mm"
                ])
     """       



"""oEditor.CreateCylinder(
	[
		"NAME:CylinderParameters",
		"XCenter:="		, "-0.8mm",
		"YCenter:="		, "-0.6mm",
		"ZCenter:="		, "-1.4mm",
		"Radius:="		, "0.115470053837925mm",
		"Height:="		, "1.4mm",
		"WhichAxis:="		, "Z",
		"NumSides:="		, "0"
	], 
	[
		"NAME:Attributes",
		"Name:="		, "Cylinder1",
		"Flags:="		, "",
		"Color:="		, "(143 175 143)",
		"Transparency:="	, 0,
		"PartCoordinateSystem:=", "Global",
		"UDMId:="		, "",
		"MaterialValue:="	, "\"vacuum\"",
		"SurfaceMaterialValue:=", "\"\"",
		"SolveInside:="		, True,
		"ShellElement:="	, False,
		"ShellElementThickness:=", "0mm",
		"IsMaterialEditable:="	, True,
		"UseMaterialAppearance:=", False,
		"IsLightweight:="	, False
	])
"""