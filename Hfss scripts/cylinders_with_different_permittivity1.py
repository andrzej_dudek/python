# ----------------------------------------------
# Script Recorded by Ansys Electronics Desktop Version 2024.1.0
# 8:42:53  May 08, 2024
# ----------------------------------------------
import ScriptEnv
import math

def calculate_permittivity(radius):
    permittivity = math.sqrt(11.93 / math.cosh(2 * math.pi / 4500 * radius))
    return permittivity


ScriptEnv.Initialize("Ansoft.ElectronicsDesktop")
oDesktop.RestoreWindow()
oProject = oDesktop.SetActiveProject("Lens_new_design")
oDesign = oProject.SetActiveDesign("BowTie6_8")
oEditor = oDesign.SetActiveEditor("3D Modeler")
oEditor.CreateCylinder(
	[
		"NAME:CylinderParameters",
		"XCenter:="		, "0um",
		"YCenter:="		, "0um",
		"ZCenter:="		, "-SiThick",
		"Radius:="		, "small_radius",
		"Height:="		, "-SiLensThick",
		"WhichAxis:="		, "Z",
		"NumSides:="		, "0"
	], 
	[
		"NAME:Attributes",
		"Name:="		, "Cylinder1",
		"Flags:="		, "",
		"Color:="		, "(143 175 143)",
		"Transparency:="	, 0,
		"PartCoordinateSystem:=", "Global",
		"UDMId:="		, "",
		"MaterialValue:="	, "\"vacuum\"",
		"SurfaceMaterialValue:=", "\"\"",
		"SolveInside:="		, True,
		"ShellElement:="	, False,
		"ShellElementThickness:=", "0um",
		"ReferenceTemperature:=", "20cel",
		"IsMaterialEditable:="	, True,
		"UseMaterialAppearance:=", False,
		"IsLightweight:="	, False
	])
oEditor.AssignMaterial(
	[
		"NAME:Selections",
		"AllowRegionDependentPartSelectionForPMLCreation:=", True,
		"AllowRegionSelectionForPMLCreation:=", True,
		"Selections:="		, "Cylinder1"
	], 
	[
		"NAME:Attributes",
		"MaterialValue:="	, "\"IHP_Si_HRes\"",
		"SolveInside:="		, True,
		"ShellElement:="	, False,
		"ShellElementThickness:=", "nan ",
		"ReferenceTemperature:=", "nan ",
		"IsMaterialEditable:="	, True,
		"UseMaterialAppearance:=", False,
		"IsLightweight:="	, False
	])
    
for i in range(7):
    ring_number = i+1
    oEditor.Copy(
        [
            "NAME:Selections",
            "Selections:="		, "Cylinder1"
        ])
    oEditor.Paste()
    oEditor.ChangeProperty(
        [
            "NAME:AllTabs",
            [
                "NAME:Geometry3DCmdTab",
                [
                    "NAME:PropServers", 
                    "Cylinder"+str(i+2)+":CreateCylinder:1"
                ],
                [
                    "NAME:ChangedProps",
                    [
                        "NAME:Radius",
                        "Value:="		, str(1 + 2 * ring_number)+"*small_radius"
                    ]
                ]
            ]
        ])
    oDefinitionManager = oProject.GetDefinitionManager()
    oDefinitionManager.AddMaterial(
        [
            "NAME:IHP_Si_HRes_Ring" + str(ring_number),
            "CoordinateSystemType:=", "Cartesian",
            "BulkOrSurfaceType:="	, 1,
            [
                "NAME:PhysicsTypes",
                "set:="			, ["Electromagnetic"]
            ],
            "permittivity:="	, str(calculate_permittivity(2 * ring_number)),
            "conductivity:="	, "0",
            "dielectric_loss_tangent:=", "0"
        ])
    oEditor.AssignMaterial(
        [
            "NAME:Selections",
            "AllowRegionDependentPartSelectionForPMLCreation:=", True,
            "AllowRegionSelectionForPMLCreation:=", True,
            "Selections:="		, "Cylinder" + str(i+2)
        ], 
        [
            "NAME:Attributes",
            "MaterialValue:="	, "\"IHP_Si_HRes_Ring" + str(ring_number) + "\"",
            "SolveInside:="		, True,
            "ShellElement:="	, False,
            "ShellElementThickness:=", "nan ",
            "ReferenceTemperature:=", "nan ",
            "IsMaterialEditable:="	, True,
            "UseMaterialAppearance:=", False,
            "IsLightweight:="	, False
        ])
