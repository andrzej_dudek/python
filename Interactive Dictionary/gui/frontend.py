from backend import Dictionary
from tkinter import *

dictionary = Dictionary("data.json")

class Window(object):

    def __init__(self, window):
        self.window = window
        self.window.wm_title("Dictionary")

        lt = Label(self.window, text=' ')
        ll = Label(self.window, text=' ')
        lp = Label(self.window, text=' ')
        lb = Label(self.window, text=' ')
        lr = Label(self.window, text=' ')

        ll.grid(row=0,column=0,rowspan=9)
        lt.grid(row=0,column=1,columnspan=5)
        lp.grid(row=2,column=1,columnspan=5)
        lb.grid(row=8,column=1,columnspan=5)
        lr.grid(row=0,column=6,rowspan=9)

        self.keyword = StringVar()
        self.e_key_search = Entry(window,textvariable=self.keyword,width=30,font=("Arial", 13))
        self.e_key_search.grid(row=1,column=1,columnspan=2)

        search_button = Button(window,text='Search',width=12,command=self.search_command,font=("Arial", 13))
        search_button.grid(row=1,column=3)

        self.window.bind('<Return>', self.search_command)

        self.t = Text(window,width=60,height=20,font=("Arial", 13))
        self.t.grid(row=3,column=1,columnspan=3,rowspan=5)


    def text_formating(self,text):
        text = text.split()
        counter = 0
        formated = ""
        for word in text:
            if counter + len(word) + 1 < 57:
                formated = formated + " " + word
                counter = counter + 1 + len(word)
            else:
                formated = formated + "\n     " + word
                counter = len(word) + 4
        return formated

    def print_in_t(self,text):
        counter = 1
        if type(text) == str:
            self.t.insert(END,self.text_formating(text))
            self.t.insert(END,":\n\n")
        else:
            for item in text:
                self.t.insert(END, (str(counter) + ". " + self.text_formating(item) + "\n"))
                self.t.insert(END,"\n")
                counter += 1

    def search_command(self,event=None):
        self.t.delete(1.0,END)
        word = self.keyword.get()
        definition = dictionary.search(word)
        if definition == 0:
            word = dictionary.else_search(word)
            if word == 0:
                top = Toplevel()
                top.title("Helper")
                m = "Sorry,\nwe couldn\'t find anything similar so please check your entry."
                msg = Message(top, text=m,font=("Arial", 13))
                msg.pack()
                okay_button = Button(top, text='Okay', command=top.destroy,font=("Arial", 13))
                okay_button.pack()
            else:
                top = Toplevel(width=100,height=100)
                top.title("Helper")
                m = "Did you mean {}?\nIf yes click Okay and retype it.\nIf no please double check the word.".format(word)
                msg = Message(top,text=m,font=("Arial", 13))
                msg.pack()
                okay_button = Button(top, text='Okay', command=top.destroy,font=("Arial", 13))
                okay_button.pack()
        else:
            self.print_in_t(self.keyword.get())
            self.print_in_t(definition)


window = Tk()
Window(window)
window.mainloop()
