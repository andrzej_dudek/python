import json
import difflib

class Dictionary:

    def __init__(self, filepath):
        self.data = json.load(open(filepath))

    def search(self,word):
        word = word.lower()

        if word in self.data.keys():
            return self.data[word]

        elif word.title() in self.data.keys():
            return self.data[word.title()]

        elif word.upper() in self.data.keys():
            return self.data[word.upper()]

        else:
            return 0

    def else_search(self, word):
        word.lower()
        word = difflib.get_close_matches(word, self.data.keys(), 1)

        if len(word) == 1:
            return word[0]
        else:
            return 0
