import json
import glob2
import difflib

def get_data_json_format():
    filename = glob2.glob("*.json")

    if len(filename) > 1:
        print("There is more than one .json file.\nPlease make sure that only one .json file is in the aplication directory.")
        print("For now your data is the first .json file in alphabetic order")
        data = json.load(open(filename[0]))

    else:
        data = json.load(open(filename[0]))

    return data

def return_definition(word, data):
    word = word.lower()

    if word in data:
        return data[word]

    elif word.title() in data:
        return data[word.title()]

    elif word.upper() in data:
        return data[word.upper()]

    else:
        word = difflib.get_close_matches(word, data.keys(), 1)

        if len(word) == 1:
            print("Did you mean {}?".format(word[0]))
            condition = True

            while(condition):
                decision = input("Please select [Y/y] if yes or [N/n] if no: ")
                decision = decision.lower()

                if decision == "y":
                    condition = False
                    return data[word[0]]

                elif decision == "n":
                    condition = False
                    return "There is no word like this in the dictionary.\nPlease double check it"

                else:
                    print("Sorry invalid option.")
        else:
            return "There is no word like this in the dictionary.\nPlease double check it."

def printing_definition(definition):
    if type(definition) == str:
        print(definition)
    else:
        num = 1
        for item in definition:
            message = str(num) + ". " + item
            print(message)
            num += 1


### Program sequence

data = get_data_json_format()
word = input("Enter word: ")
printing_definition(return_definition(word, data))
