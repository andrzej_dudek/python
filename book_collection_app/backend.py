#
# Module which uses sqlite3 module to comunicate with sqlite database.
#

import sqlite3

#
# Database class to make object to operate connection to database
# and to operate cursor in database.
#
class Database:

    #
    # This function initializes the Database object
    # creates a connection to the database file
    # creates a cursor in database
    # and creates a table in database if it doesn't esist.
    # The function has one parameter called db which is the name of the database file.
    #
    def __init__(self, db="books.db"):
        self.conn = sqlite3.connect(db)
        self.cur = self.conn.cursor()
        self.cur.execute("CREATE TABLE IF NOT EXISTS book (id INTEGER PRIMARY KEY AUTOINCREMENT, title TEXT, author TEXT, year INTEGER, isbn TEXT)")
        self.conn.commit()

    #
    # This function is used to insert entry to the database file.
    # It has 4 parameters title, author, year and isbn:
    # - title is a string in which we give the title of the book
    # - author is a string which contains the name of the author of the book
    # - year is an integer which contains the year in which the book was published
    # - isbn is a string which contains the isbn number
    #
    def insert(self, title, author, year, isbn):
        self.cur.execute("INSERT INTO book VALUES (NULL,?,?,?,?)",(title,author,year,isbn))
        self.conn.commit()

    #
    # This function is used to view the whole database.
    # It returns all entries to the database.
    #
    def view(self):
        self.cur.execute("SELECT * FROM book")
        rows = self.cur.fetchall()
        return rows

    #
    # This function is used to search entry in the database file.
    # It has 4 parameters title, author, year and isbn:
    # - title is a string in which we give the title of the book
    # - author is a string which contains the name of the author of the book
    # - year is an integer which contains the year in which the book was published
    # - isbn is a string which contains the isbn number
    # It returns all entries which match the filter of parameters used by user.
    #
    def search(self, title="", author="", year="", isbn=""):

        if (len(author) == 0 and len(year) == 0 and len(isbn) == 0) and len(title) > 0:
            self.cur.execute("SELECT * FROM book WHERE title=?",(title,))
        elif (len(title) == 0 and len(year) == 0 and len(isbn) == 0) and len(author) > 0:
            self.cur.execute("SELECT * FROM book WHERE author=?",(author,))
        elif (len(title) == 0 and len(author) == 0 and len(isbn) == 0) and len(year) > 0:
            self.cur.execute("SELECT * FROM book WHERE year=?",(year,))
        elif (len(title) == 0 and len(author) == 0 and len(year) == 0) and len(isbn) > 0:
            self.cur.execute("SELECT * FROM book WHERE isbn=?",(isbn,))
        elif (len(title) > 0 and len(author) > 0 and len(year) == 0 and len(isbn) == 0):
            self.cur.execute("SELECT * FROM book WHERE title=? AND author=?",(title,author))
        else:
            self.cur.execute("SELECT * FROM book WHERE title=? OR author=? OR year=? OR isbn=?",(title,author,year,isbn))

        rows = self.cur.fetchall()
        return rows

    #
    # This function is used to delete the entry from the databse file.
    # It has one parameter id which is an integer and it is the primary key of
    # the database entry which user wants to delete from the database.
    #
    def delete(self, id):
        self.cur.execute("DELETE FROM book WHERE id=?",(id,))
        self.conn.commit()

    #
    # This function is used to update the entry in databasae file.
    # It has 5 parameters title, author, year and isbn:
    # - id is an integer which contains the primary key of the updated entry
    # - title is a string in which we give the title of the book
    # - author is a string which contains the name of the author of the book
    # - year is an integer which contains the year in which the book was published
    # - isbn is a string which contains the isbn number
    #
    def update(self, id, title, author, year, isbn):
        self.cur.execute("UPDATE book SET title=?, author=?, year=?, isbn=? WHERE id=?",(title,author,year,isbn,id))
        self.conn.commit()

    #
    # This function is used to close connection to the database.
    # It is called when the Database object is deleted so in this case
    # the function is called when the program ends.
    #
    def __del__(self):
        self.conn.close()
