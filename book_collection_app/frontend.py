"""
A program that stores this book information:
title, author, year, ISBN

User can:
View all records
Search an entry
Add entry
Update entry
Delete
Close
"""
# Program will use modules:
# - tkinter to make gui
# - backend to interact with database
from tkinter import *
# from backend we import Database class
from backend import Database

# we are creating Database object with name database.
database = Database("books.db")
#
# we are creating a class Window which inherits from class object from
# tkinter module
#
class Window(object):

    #
    # This function is used to create a Window object.
    #
    def __init__(self, window):

        self.window = window
        # giving the name to the window.
        self.window.wm_title("Book Collection")

        # creation of label type widgets in specified positions
        # with specified text.
        label_title = Label(window, text='Title',width=6)
        label_title.grid(row=0,column=0)
        label_year = Label(window, text='Year',width=6)
        label_year.grid(row=1,column=0)
        label_author = Label(window, text='Author')
        label_author.grid(row=0,column=2)
        label_isbn = Label(window, text='ISBN')
        label_isbn.grid(row=1,column=2)

        # creation of variables for Entry widgets.
        self.title_text = StringVar()
        self.year_text = StringVar()
        self.author_text = StringVar()
        self.isbn_text = StringVar()
        # creation of Entry widgets in specified positions
        # with specified variables for text which will be entered by the user.
        self.e_title = Entry(window, textvariable=self.title_text)
        self.e_author = Entry(window, textvariable=self.author_text)
        self.e_year = Entry(window, textvariable=self.year_text)
        self.e_isbn = Entry(window, textvariable=self.isbn_text)
        self.e_title.grid(row=0,column=1)
        self.e_year.grid(row=1,column=1)
        self.e_author.grid(row=0,column=3)
        self.e_isbn.grid(row=1,column=3)

        # creation Listbox widget and scrollbar to use it with the Listbox
        # in specified positions
        scrollbar = Scrollbar(window)
        self.listbox = Listbox(window, width=30)
        self.listbox.grid(row=2,column=0,rowspan=6,columnspan=2)
        scrollbar.grid(row=2,column=2,rowspan=6)
        self.listbox.configure(yscrollcommand=scrollbar.set)
        scrollbar.configure(command=self.listbox.yview)

        # adding an event for a click on one row in Listbox widget.
        self.listbox.bind('<<ListboxSelect>>', self.get_selected_row)

        # creation of Button widgets wit specified text, position and method
        b_view_all = Button(window,text='View all',width=12,command=self.view_command)
        b_search_entry = Button(window,text='Search entry',width=12,command=self.search_command)
        b_add_entry = Button(window,text='Add entry',width=12,command=self.add_command)
        b_update = Button(window,text='Update selected',width=12,command=self.update_command)
        b_delete = Button(window,text='Delete selected',width=12,command=self.delete_command)
        b_close = Button(window,text='Close',width=12,command=window.destroy)
        b_view_all.grid(row=2,column=3)
        b_search_entry.grid(row=3,column=3)
        b_add_entry.grid(row=4,column=3)
        b_update.grid(row=5,column=3)
        b_delete.grid(row=6,column=3)
        b_close.grid(row=7,column=3)

    #
    # This function is called when the <<ListboxSelect>> event occurs.
    # As a result the Entry widgets are cleared and then inserted with
    # the values from the selected row in Listbox widget.
    #
    def get_selected_row(self, event):
        try:
            index = self.listbox.curselection()[0]
            self.selected_tuple = self.listbox.get(index)
            self.e_title.delete(0,END)
            self.e_title.insert(END,self.selected_tuple[1])
            self.e_author.delete(0,END)
            self.e_author.insert(END,self.selected_tuple[2])
            self.e_year.delete(0,END)
            self.e_year.insert(END,self.selected_tuple[3])
            self.e_isbn.delete(0,END)
            self.e_isbn.insert(END,self.selected_tuple[4])
        except IndexError:
            pass

    #
    # This function is called when user clicks on View All Button.
    # As a result the listbox is cleared and then it is inserted with all
    # databse entries
    #
    def view_command(self):
        self.listbox.delete(0,END)
        for row in database.view():
            self.listbox.insert(END,row)

    #
    # This function is called when user clicks on Search Button.
    # As a result the listbox is cleared and then it is inserted with all
    # databse entries which passed the user's filter.
    #
    def search_command(self):
        self.listbox.delete(0,END)
        for row in database.search(self.title_text.get(), self.author_text.get(), self.year_text.get(), self.isbn_text.get()):
            self.listbox.insert(END,row)

    #
    # This function is called when user clicks on Add entry Button.
    # As a result the listbox is cleared and then it is inserted with
    # the new entry which is also added to database.
    #
    def add_command(self):
        database.insert(self.title_text.get(), self.author_text.get(), self.year_text.get(), self.isbn_text.get())
        self.listbox.delete(0,END)
        self.listbox.insert(END,(1,self.title_text.get(), self.author_text.get(), self.year_text.get(), self.isbn_text.get()))

    #
    # This function is called when user clicks on Delete selected Button.
    # As a result the entry is deleted from the database
    #
    def delete_command(self):
        database.delete(self.selected_tuple[0])

    #
    # This function is called when user clicks on Update selected Button.
    # As a result the entry is updated in the database with the values from the
    # Entry widgets.
    #
    def update_command(self):
        database.update(self.selected_tuple[0], self.title_text.get(), self.author_text.get(), self.year_text.get(), self.isbn_text.get())


# creation of the Tk object called window
window = Tk()
# calling the constructor from Window class
# and ensuring continues work of program
Window(window)
window.mainloop()
