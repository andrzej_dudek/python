import time
from datetime import datetime as dt

hosts_temp = "/Users/andrzejdudek/Documents/Python/python/website\ blocker"
hosts_path = "/etc/hosts"
redirect = "127.0.0.1"
website_list = ["www.facebook.com", "facebook.com", "www.instagram.com",
"instagram.com"]

while True:
    if (dt(dt.now().year, dt.now().month, dt.now().day, 8) <  dt.now() <
    dt(dt.now().year, dt.now().month, dt.now().day, 16)):
        with open(hosts_path, "r+") as file:
            content = file.read()
            for item in website_list:
                if item in content:
                    continue
                else:
                    file.write(redirect + "\t" + item + "\n")
    else:
        with open(hosts_path, 'r+') as file:
            content = file.readlines()
            file.seek(0)
            for line in content:
                if not any(item in line for item in website_list):
                    file.write(line)
            file.truncate()

    time.sleep(5)
