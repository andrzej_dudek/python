from flask import Flask, render_template, request
from werkzeug import secure_filename
from geo_code import search_for, geocode_csv

app = Flask(__name__)

@app.route('/')
def index():
    return render_template("index.html")

@app.route("/success/", methods=['POST'])
def success():
    global file
    if request.method=='POST':
        file = request.files["file"]
        file.save(secure_filename("uploaded"+file.filename))
        if search_for("uploaded"+file.filename) == 1:
            file = geocode_csv("uploaded"+file.filename)
            return render_template("success.html", btn="download.html")
        else:
            return render_template("index.html",
                                   text="Please make sure that is address or Address column in your CSV file.")

@app.route("/download")
def download():
    return send_file("uploaded"+file.filename, attachment_filename="yourfile.csv")

if __name__ == '__main__':
    app.debug = True
    app.run()
