from geopy.geocoders import ArcGIS
nom = ArcGIS()
import pandas

def search_for(file):
    df = pandas.read_csv(open(file))
    try:
        if 'address' in df.columns or 'Address' in df.columns:
            return 1
    except KeyError:
        return 0

def geocode_csv(file):
    df = pandas.read_csv(open(file))
    if 'address' in df.axes[1]:
        df['lon'] = df['address'].apply(nom.geocode).longitude #.apply(lambda x: x.longitude if x != None else None)
        df['lat'] = df['address'].apply(nom.geocode).latitude #.aply(lambda x: x.latitude if x != None else None)
    else:
        df['lon'] = df['Address'].apply(nom.geocode).longitude
        df['lat'] = df['Address'].apply(nom.geocode).latitude

    df.to_csv(file)
    return file
