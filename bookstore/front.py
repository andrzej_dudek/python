"""
A program that stores this book information:
title, author, year, ISBN

User can:
View all records
Search an entry
Add entry
Update entry
Delete
Close
"""
from tkinter import *
import back

def get_selected_row(event):
    try:
        global selected_tuple
        index = listbox.curselection()[0]
        selected_tuple = listbox.get(index)
        e_title.delete(0,END)
        e_title.insert(END,selected_tuple[1])
        e_author.delete(0,END)
        e_author.insert(END,selected_tuple[2])
        e_year.delete(0,END)
        e_year.insert(END,selected_tuple[3])
        e_isbn.delete(0,END)
        e_isbn.insert(END,selected_tuple[4])
    except IndexError:
        pass

def view_command():
    listbox.delete(0,END)
    for row in back.view():
        listbox.insert(END,row)

def search_command():
    listbox.delete(0,END)
    for row in back.search(title_text.get(), author_text.get(), year_text.get(), isbn_text.get()):
        listbox.insert(END,row)

def add_command():
    back.insert(title_text.get(), author_text.get(), year_text.get(), isbn_text.get())
    listbox.delete(0,END)
    listbox.insert(END,(title_text.get(), author_text.get(), year_text.get(), isbn_text.get()))

def delete_command():
    back.delete(selected_tuple[0])

def update_command():
    back.update(selected_tuple[0], title_text.get(), author_text.get(), year_text.get(), isbn_text.get())

window = Tk()

window.wm_title("Books")

label_title = Label(window, text='Title',width=6)
label_title.grid(row=0,column=0)
label_year = Label(window, text='Year',width=6)
label_year.grid(row=1,column=0)
label_author = Label(window, text='Author')
label_author.grid(row=0,column=2)
label_isbn = Label(window, text='ISBN')
label_isbn.grid(row=1,column=2)

title_text = StringVar()
year_text = StringVar()
author_text = StringVar()
isbn_text = StringVar()
e_title = Entry(window, textvariable=title_text)
e_year = Entry(window, textvariable=year_text)
e_author = Entry(window, textvariable=author_text)
e_isbn = Entry(window, textvariable=isbn_text)
e_title.grid(row=0,column=1)
e_year.grid(row=1,column=1)
e_author.grid(row=0,column=3)
e_isbn.grid(row=1,column=3)

scrollbar = Scrollbar(window)
listbox = Listbox(window, width=30)
listbox.grid(row=2,column=0,rowspan=6,columnspan=2)
scrollbar.grid(row=2,column=2,rowspan=6)
listbox.configure(yscrollcommand=scrollbar.set)
scrollbar.configure(command=listbox.yview)

listbox.bind('<<ListboxSelect>>', get_selected_row)

b_view_all = Button(window,text='View all',width=12,command=view_command)
b_search_entry = Button(window,text='Search entry',width=12,command=search_command)
b_add_entry = Button(window,text='Add entry',width=12,command=add_command)
b_update = Button(window,text='Update selected',width=12,command=update_command)
b_delete = Button(window,text='Delete selected',width=12,command=delete_command)
b_close = Button(window,text='Close',width=12,command=window.destroy)
b_view_all.grid(row=2,column=3)
b_search_entry.grid(row=3,column=3)
b_add_entry.grid(row=4,column=3)
b_update.grid(row=5,column=3)
b_delete.grid(row=6,column=3)
b_close.grid(row=7,column=3)

window.mainloop()
