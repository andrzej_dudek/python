"""
A program that stores this book information:
title, author, year, ISBN

User can:
View all records
Search an entry
Add entry
Update entry
Delete
Close
"""
from tkinter import *
from backend import Database

database = Database("books.db")

class Window(object):

    def __init__(self, window):

        self.window = window
        self.window.wm_title("Books")

        label_title = Label(window, text='Title',width=6)
        label_title.grid(row=0,column=0)
        label_year = Label(window, text='Year',width=6)
        label_year.grid(row=1,column=0)
        label_author = Label(window, text='Author')
        label_author.grid(row=0,column=2)
        label_isbn = Label(window, text='ISBN')
        label_isbn.grid(row=1,column=2)

        self.title_text = StringVar()
        self.year_text = StringVar()
        self.author_text = StringVar()
        self.isbn_text = StringVar()
        self.e_title = Entry(window, textvariable=self.title_text)
        self.e_author = Entry(window, textvariable=self.author_text)
        self.e_year = Entry(window, textvariable=self.year_text)
        self.e_isbn = Entry(window, textvariable=self.isbn_text)
        self.e_title.grid(row=0,column=1)
        self.e_year.grid(row=1,column=1)
        self.e_author.grid(row=0,column=3)
        self.e_isbn.grid(row=1,column=3)

        scrollbar = Scrollbar(window)
        self.listbox = Listbox(window, width=30)
        self.listbox.grid(row=2,column=0,rowspan=6,columnspan=2)
        scrollbar.grid(row=2,column=2,rowspan=6)
        self.listbox.configure(yscrollcommand=scrollbar.set)
        scrollbar.configure(command=self.listbox.yview)

        self.listbox.bind('<<ListboxSelect>>', self.get_selected_row)

        b_view_all = Button(window,text='View all',width=12,command=self.view_command)
        b_search_entry = Button(window,text='Search entry',width=12,command=self.search_command)
        b_add_entry = Button(window,text='Add entry',width=12,command=self.add_command)
        b_update = Button(window,text='Update selected',width=12,command=self.update_command)
        b_delete = Button(window,text='Delete selected',width=12,command=self.delete_command)
        b_close = Button(window,text='Close',width=12,command=window.destroy)
        b_view_all.grid(row=2,column=3)
        b_search_entry.grid(row=3,column=3)
        b_add_entry.grid(row=4,column=3)
        b_update.grid(row=5,column=3)
        b_delete.grid(row=6,column=3)
        b_close.grid(row=7,column=3)



    def get_selected_row(self, event):
        try:
            index = self.listbox.curselection()[0]
            self.selected_tuple = self.listbox.get(index)
            self.e_title.delete(0,END)
            self.e_title.insert(END,self.selected_tuple[1])
            self.e_author.delete(0,END)
            self.e_author.insert(END,self.selected_tuple[2])
            self.e_year.delete(0,END)
            self.e_year.insert(END,self.selected_tuple[3])
            self.e_isbn.delete(0,END)
            self.e_isbn.insert(END,self.selected_tuple[4])
        except IndexError:
            pass

    def view_command(self):
        self.listbox.delete(0,END)
        for row in database.view():
            self.listbox.insert(END,row)

    def search_command(self):
        self.listbox.delete(0,END)
        for row in database.search(self.title_text.get(), self.author_text.get(), self.year_text.get(), self.isbn_text.get()):
            self.listbox.insert(END,row)

    def add_command(self):
        database.insert(self.title_text.get(), self.author_text.get(), self.year_text.get(), self.isbn_text.get())
        self.listbox.delete(0,END)
        self.listbox.insert(END,(1,self.title_text.get(), self.author_text.get(), self.year_text.get(), self.isbn_text.get()))

    def delete_command(self):
        database.delete(self.selected_tuple[0])

    def update_command(self):
        database.update(self.selected_tuple[0], self.title_text.get(), self.author_text.get(), self.year_text.get(), self.isbn_text.get())



window = Tk()
Window(window)
window.mainloop()
