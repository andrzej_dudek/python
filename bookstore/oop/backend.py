import sqlite3

class Database:

    def __init__(self, db):
        self.conn = sqlite3.connect("books.db")
        self.cur = self.conn.cursor()
        self.cur.execute("CREATE TABLE IF NOT EXISTS book (id INTEGER PRIMARY KEY AUTOINCREMENT, title TEXT, author TEXT, year INTEGER, isbn TEXT)")
        self.conn.commit()

    def insert(self, title, author, year, isbn):
        self.cur.execute("INSERT INTO book VALUES (NULL,?,?,?,?)",(title,author,year,isbn))
        self.conn.commit()

    def view(self):
        self.cur.execute("SELECT * FROM book")
        rows = self.cur.fetchall()
        return rows

    def search(self, title="", author="", year="", isbn=""):

        if (len(author) == 0 and len(year) == 0 and len(isbn) == 0) and len(title) > 0:
            self.cur.execute("SELECT * FROM book WHERE title=?",(title,))
        elif (len(title) == 0 and len(year) == 0 and len(isbn) == 0) and len(author) > 0:
            self.cur.execute("SELECT * FROM book WHERE author=?",(author,))
        elif (len(title) == 0 and len(author) == 0 and len(isbn) == 0) and len(year) > 0:
            self.cur.execute("SELECT * FROM book WHERE year=?",(year,))
        elif (len(title) == 0 and len(author) == 0 and len(year) == 0) and len(isbn) > 0:
            self.cur.execute("SELECT * FROM book WHERE isbn=?",(isbn,))
        elif (len(title) > 0 and len(author) > 0 and len(year) == 0 and len(isbn) == 0):
            self.cur.execute("SELECT * FROM book WHERE title=? AND author=?",(title,author))
        else:
            self.cur.execute("SELECT * FROM book WHERE title=? OR author=? OR year=? OR isbn=?",(title,author,year,isbn))

        rows = self.cur.fetchall()
        return rows

    def delete(self, id):
        self.cur.execute("DELETE FROM book WHERE id=?",(id,))
        self.conn.commit()

    def update(self, id, title, author, year, isbn):
        self.cur.execute("UPDATE book SET title=?, author=?, year=?, isbn=? WHERE id=?",(title,author,year,isbn,id))
        self.conn.commit()

    def __del__(self):
        self.conn.close()
