import folium
import pandas
map = folium.Map(location=[50.06, 19.95], zoom_start=7)
#folium.TileLayer("stamentoner").add_to(map)
#folium.TileLayer("Mapbox Bright").add_to(map)

fgtile = folium.FeatureGroup(name="cartodbpositron")
fgtile.add_child(folium.TileLayer("cartodbpositron"))
map.add_child(fgtile)

data = pandas.read_csv("Volcanoes.txt")
lat = list(data["LAT"])
lon = list(data["LON"])
name = list(data["NAME"])

fgpoint = folium.FeatureGroup(name="Point Layer")
fgpoint.add_child(folium.Marker(location=[49.96, 20.83], popup="Hi from Wojnicz",
icon=folium.Icon(color="green")))

for lt, ln, nm in zip(lat, lon, name):
    fgpoint.add_child(folium.CircleMarker(location=[lt,ln], popup=nm, radius=6,
    fill=True, fill_color='red', fill_opacity=0.9, color='grey'))

map.add_child(fgpoint)

fgpolygon = folium.FeatureGroup(name="Polygon Layer")

fgpolygon.add_child(folium.GeoJson(data=open("world.json", 'r',
encoding='utf-8-sig').read(),
style_function=lambda x: {'fillColor':'green' if x['properties']['POP2005'] < 10000000
else 'yellow' if 10000000 <= x['properties']['POP2005'] < 30000000
else 'orange' if 30000000 <= x['properties']['POP2005'] < 50000000
else 'red'}))

map.add_child(fgpolygon)

map.add_child(folium.LayerControl())

map.save("Map1.html")
# Cracow [50.06, 19.95]
