import pandas
import folium

map = folium.Map(location=[54.5260, 15.2551], zoom_start = 4)

fgp = folium.FeatureGroup(name="Some places I've been")

data = pandas.read_csv("places.txt")
lat = list(data["LAT"])
lon = list(data["LON"])
data["NAME"] = data["NAME"] + ", " + data["YEAR"]
name = list(data["NAME"])

for lt, ln, nm in zip(lat, lon, name):
    fgp.add_child(folium.CircleMarker(location=[lt,ln], popup=name, radius=6,
    fill=True, fill_color='yellow', color='grey', fill_opacity=0.9))

map.add_child(fgp)

fgt1 = folium.FeatureGroup(name="natgeo")
fgt1.add_child(folium.TileLayer("https://server.arcgisonline.com/ArcGIS/rest/services/NatGeo_World_Map/MapServer/tile/{z}/{y}/{x}'))",
attr='Tiles &copy; Esri &mdash; National Geographic, Esri, DeLorme, NAVTEQ, UNEP-WCMC, USGS, NASA, ESA, METI, NRCAN, GEBCO, NOAA, iPC'))
map.add_child(fgt1)

#fgt2 = folium.FeatureGroup(name="cartodbpositron")
#fgt2.add_child(folium.TileLayer("cartodbpositron"))
#map.add_child(fgt2)

#fgt3 = folium.FeatureGroup(name="openstreetmap")
#fgt3.add_child(folium.TileLayer("openstreetmap"))
#map.add_child(fgt3)

map.add_child(folium.LayerControl())
map.save("Personal_Map.html")
