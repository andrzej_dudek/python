import struct

TAB_1 = "\t - "
TAB_2 = "\t\t - "
TAB_3 = "\t\t\t - "
TAB_4 = "\t\t\t\t - "

DATA_TAB_1 = "\t "
DATA_TAB_2 = "\t\t "
DATA_TAB_3 = "\t\t\t "
DATA_TAB_4 = "\t\t\t\t "

PROTOCOLV4 = 8
PROTOCOLV6 = 56710

#
# This function is used to get information from ipv4 header.
# It has 1 parameter:
# - data - whole ipv4 packet.
# It returns:
# - version - version of IP
# - header_length - length of IPv4 header
# - ttl - time to live from header
# - proto - protocol of 4th layer
# - source address (3rd layer)
# - destination address (3rd layer)
# - data - data from IPv4 packet
#
def ipv4_packet(data):
    version_header_length = data[0]
    version = version_header_length >> 4
    header_length = (version_header_length & 15) * 4
    ttl, proto, source, target = struct.unpack('! 8x B B 2x 4s 4s', data[:20])
    return version, header_length, ttl, proto, ipv4(source), ipv4(target), data[header_length:]

#
# This function is used to make IPv4 address readable to humans.
# It has 1 paramter:
# - addr - IP address
# It returns:
# - ip_addr - IP address in readalbe form
#
def ipv4(addr):
    ip_addr = '.'.join(map(str, addr))
    return ip_addr

#
# This function is used to print information form IPv4 header.
# It has 1 paramter:
# - data - whole IPv4 packet
# It returns:
# - proto - 4th layer protocol
# - data - 4th layer datagram/packet
#
def print_ipv4(data):
    version, header_length, ttl, proto, src_addr, dest_addr, data = ipv4_packet(data)
    print(TAB_1 + 'IPv4 packet: ')
    print(TAB_2 + 'Version: {}, Header Length: {}, TTL: {}'.format(version, header_length, ttl))
    print(TAB_2 + 'Protocol: {}, Source: {}, Destination: {}'.format(proto, src_addr, dest_addr))
    return proto, data

#
# This function is used to get information from IPv6 header
# It has 1 paramter:
# - data - IPv6 packet
# It returns:
# - version - version of IP protocol
# - traffic_class - class of traffic
# - flow label - flow label
# - payload_length - length of payload
# - next_header - protocol of 4th layer
# - hop_limit - limit of hops
# - source - source IPv6 address
# - destination - destination IPv6 address
# - data - 4th layer datagram/packet
#
def ipv6_packet(data):
    version = data[0] >> 4
    traffic_class = ((data[0] & 15) << 4) | (data[1] >> 4)
    flow_label = ( (data[1] & 15) << 16 ) | ((data[2] & 255) << 8) | data[3]
    payload_length = ((data[4] & 255) << 8) | data[5]
    next_header = data[6]
    hop_limit = data[7]
    source, target = struct.unpack('! 16s 16s', data[8:40])
    data = data[40:]
    return (version, traffic_class, flow_label,
            payload_length, next_header, hop_limit,
            ipv6(source), ipv6(target), data)

#
# This function is used to make IPv6 address readable.
# It has 1 parameter:
# - bytes_addr - address in bytes
# It returns:
# - ip_addr - IPv6 address in readable way.
#
def ipv6(bytes_addr):
    bytes_str = map('{:02X}'.format, bytes_addr)
    ip_addr = ':'.join(bytes_str)
    return ip_addr

#
# This function is used to print information form IPv6 header.
# It has 1 paramter:
# - data - whole IPv6 packet
# It returns:
# - next_header - 4th layer protocol
# - data - 4th layer datagram/packet
#
def print_ipv6(data):
    (version, traffic_class, flow_label,
     payload_length, next_header, hop_limit,
     source, destination, data) = ipv6_packet(data)

    print(TAB_1 + 'IPv6 packet:')
    print(TAB_2 + 'Version: {}, Traffic Class: {}'.format(version, traffic_class))
    print(TAB_2 + 'Flow Label: {}, Payload Length: {}'.format(flow_label, payload_length))
    print(TAB_2 + 'Next Header {}, Hop Limit: {}'.format(next_header, hop_limit))
    print(TAB_2 + 'Source Address: {}'.format(source))
    print(TAB_2 + 'Destination Address: {}'.format(destination))
    return next_header, data
