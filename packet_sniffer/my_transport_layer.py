import textwrap
import struct

TAB_1 = "\t - "
TAB_2 = "\t\t - "
TAB_3 = "\t\t\t - "
TAB_4 = "\t\t\t\t - "
TAB_5 = "\t\t\t\t\t - "

DATA_TAB_1 = "\t "
DATA_TAB_2 = "\t\t "
DATA_TAB_3 = "\t\t\t "
DATA_TAB_4 = "\t\t\t\t "
DATA_TAB_5 = "\t\t\t\t\t "

ICMP_PROTO = 1
TCP_PROTO = 6
UDP_PROTO = 17
ICMPV6_PROTO = 58
SCTP_PROTO = 132

#
# This function is used to print information from ICMP header and data.
# It has 1 parameter:
# - data - ICMP packet
#
def print_icmp(data):
    icmp_type, code, checksum, data = unpack_icmp(data)
    print(TAB_2 + 'ICMP Message:')
    print(TAB_3 + 'Type: {}, Code: {}, Checksum: {}'.format(icmp_type,code,checksum))
    print(TAB_3 + 'Data:')
    print(format_multi_line(DATA_TAB_4, data))
#
# This function is used to print information from ICMP6 header and data.
# It has 1 parameter:
# - data - ICMP6 packet
#
def print_icmp6(data):
    icmp_type, code, checksum, data = unpack_icmp(data)
    print(TAB_2 + 'ICMP Message:')
    print(TAB_3 + 'Type: {}, Code: {}, Checksum: {}'.format(icmp_type,code,checksum))
    print(TAB_3 + 'Data:')
    print(format_multi_line(DATA_TAB_4, data))

#
# This function is used to print information from TCP header and data of the packet.
# It has 1 parameter:
# - data - TCP packet
#
def print_tcp(data):
    (src_port, dest_port, seq_num, ack_num,
    flag_urg, flag_ack, flag_psh, flag_rst,
    flag_syn, flag_fin, data) = unpack_tcp(data)
    print(TAB_2 + 'TCP Segment:')
    print(TAB_3 + 'Source Port: {}, Destination Port: {}'.format(src_port, dest_port))
    print(TAB_3 + 'Sequence Number: {}, Acknowledgement Number: {}'.format(seq_num, ack_num))
    print(TAB_3 + 'Flags:')
    print(TAB_3 + 'URG: {}, ACK: {}, PSH: {}, RST: {}, SYN: {}, FIN {}'.format(flag_urg,flag_ack,flag_psh,flag_rst,flag_syn,flag_fin))
    print(TAB_3 + 'Data:')
    print(format_multi_line(DATA_TAB_4, data))
#
# This function is used to print data of the IP packet if the 4th layer protocol
# is unknown. (Not TCP, UDP, SCTP, ICMP, ICMP6)
#
def print_else(data):
    print(TAB_3 + 'Data:')
    print(format_multi_line(DATA_TAB_4, data))

#
# This function is used to print information from UDP header and data of the datagram.
# It has 1 parameter:
# - data - UDP datagram
#
def print_udp(data):
    src_port, dest_port, size, data = unpack_udp(data)
    print(TAB_2 + 'UDP Segment:')
    print(TAB_3 + 'Source Port: {}, Destination Port: {}'.format(src_port, dest_port))
    print(TAB_3 + 'Size: {}'.format(size))
    print(TAB_3 + 'Data:')
    print(format_multi_line(DATA_TAB_4, data))

#
# This function is used to get informations from ICMP header.
# It has 1 parameter:
# - data - ICMP packet
# It returns:
# - icmp_type - Type of ICMP message
# - code - code
# - checksum - checksum
# - data - data from ICMP packet
#
def unpack_icmp(data):
    icmp_type, code, checksum = struct.unpack('! B B H', data[:4])
    return icmp_type, code, checksum, data[4:]

#
# This function is used to get informations from TCP header.
# It has 1 parameter:
# - data - TCP packet
# It returns:
# - src_port - source port
# - dest_port - destination port
# - seq_num - sequence number
# - ack_num - acknoledgement number
# - flags from TCP header [URG, ACK, PSH, RST, SYN, FIN]
# - data - data from TCP packet
#
def unpack_tcp(data):
    (src_port, dest_port, seq_num, ack_num, offset_reserved_flags) = struct.unpack('! H H L L H', data[:14])
    offset = (offset_reserved_flags >> 12) * 4
    flag_urg = (offset_reserved_flags & 32) >> 5
    flag_ack = (offset_reserved_flags & 16) >> 4
    flag_psh = (offset_reserved_flags & 8) >> 3
    flag_rst = (offset_reserved_flags & 4) >> 2
    flag_syn = (offset_reserved_flags & 2) >> 1
    flag_fin = offset_reserved_flags & 1
    return src_port, dest_port, seq_num, ack_num, flag_urg, flag_ack, flag_psh, flag_rst, flag_syn, flag_fin, data[offset:]


#
# This function is used to get informations from UDP header.
# It has 1 parameter:
# - data - UDP datagram
# It returns:
# - src_port - source port
# - dest_port - destination port
# - size - size of UDP header
# - data - data from UDP datagram
#
def unpack_udp(data):
    src_port, dest_port, size = struct.unpack('! H H 2x H', data[:8])
    return src_port, dest_port, size, data[size:]

#
# This function is used to get informations from ICMP6 header.
# It has 1 parameter:
# - data - ICMP packet
# It returns:
# - icmp_type - Type of ICMP6 message
# - code - code
# - checksum - checksum
# - data - data from ICMP6 packet
#
def unpack_icmpv6(data):
    icmp_type, code, checksum = struct.unpack('! B B H', data[:4])
    return icmp_type, code, checksum, data[4:]

#
# This function is used to format data to print it in terminal.
# It has 3 parameters:
# - prefix - it is a DATA_TAB which we want to use
# - string - data we want to print
# - size - number of characters in one line
#
def format_multi_line(prefix, string, size=80):
    size -= len(prefix)
    if isinstance(string, bytes):
        string = ''.join(r'\x{:02x}'.format(byte) for byte in string)
        if size % 2:
            size -= 1
    return "\n".join([prefix + line for line in textwrap.wrap(string, size)])

#
# This function is used to get informations from SCTP header.
# It has 1 parameter:
# - data - SCTP packet
# It returns:
# - src_port - source port
# - dest_port - destination port
# - ver_tag - verificaton tag
# - checksum - checksum
# - data - SCTP chunks
#
def unpack_sctp(data):
    src_port, dest_port, ver_tag, checksum = struct.unpack('! H H L L', data[:12])
    return src_port, dest_port, ver_tag, checksum, data[12:]

#
# This function is used to print information from SCTP header and SCTP chunks
# It has 1 parameter:
# - data - SCTP packet
#
def print_sctp_header(data):
    src_port, dest_port, ver_tag, checksum, data = unpack_sctp(data)
    print(TAB_2 + 'SCTP packet:')
    print(TAB_3 + 'Source Port: {}, Destination Port: {}'.format(src_port, dest_port))
    print(TAB_3 + 'Verificaton tag: {}'.format(ver_tag))
    print(TAB_3 + 'Checksum: {}'.format(checksum))
    print(TAB_3 + 'Chunks:')
    print_sctp_chunk(data)

#
# This function is used to print information from SCTP chunk.
# It has 1 parameter:
# - data - SCTP chunk or chunks.
#
def print_sctp_chunk(data):
    chunk_type, chunk_flags, size = struct.unpack('! B B H', data[:4])
    print(TAB_4 + "Chunk type: {}".format(chunk_type))
    print(TAB_4 + "Flags:")
    i_flag = (chunk_flags & 8) >> 3
    u_flag = (chunk_flags & 4) >> 2
    b_flag = (chunk_flags & 2) >> 1
    e_flag = chunk_flags & 1
    print(TAB_4 + "I: {}, U: {}, B: {}, E: {}".format(i_flag, u_flag, b_flag, e_flag))
    print(format_multi_line(DATA_TAB_4, data[4:size]))

    if length(data) > size:
        print_sctp_chunk(data[size:])
