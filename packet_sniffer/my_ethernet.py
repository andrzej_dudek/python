import socket
import struct

TAB_1 = "\t - "
TAB_2 = "\t\t - "
TAB_3 = "\t\t\t - "
TAB_4 = "\t\t\t\t - "

DATA_TAB_1 = "\t "
DATA_TAB_2 = "\t\t "
DATA_TAB_3 = "\t\t\t "
DATA_TAB_4 = "\t\t\t\t "

#
# This function is used to print information about ethernet frame.
# It has 1 parameter:
# - sniffing_socket - it is a descriptor for a AF_SOCKET socket.
# It returns:
# - eth_proto - number of protocol from ethernet frame header.
# - data - bytes of frame payload
#
def print_ethernet(sniffing_socket):
    raw_data, addr = sniffing_socket.recvfrom(65536)
    dest_mac, src_mac, eth_proto, data = ethernet_frame(raw_data)
    print("\nEthernet Frame:")
    print(TAB_1 + "Dest: {}, Source: {}, Protocol: {}".format(dest_mac, src_mac, eth_proto))
    return eth_proto, data

#
# This function is used to get data from ethernet frame header.
# It has 1 parameter:
# - data - whole ethernet frame
# It returns:
# - destination address (2nd layer)
# - source address (2nd layer)
# - 3rd layer protocol from ethernet frame header
# - data - payload of ethernet frame
#
def ethernet_frame(data):
    dest_mac, src_mac, proto = struct.unpack("! 6s 6s H", data[:14])
    return get_mac_addr(dest_mac), get_mac_addr(src_mac), socket.htons(proto), data[14:]

#
# This function is used to format string of bytes into readable by humans
# 2nd layer address.
# It has 1 parameter:
# - bytes_addr - 2nd layer address in bytes
# It returns:
# - mac_addr - string readable by humans 
#
def get_mac_addr(bytes_addr):
    bytes_str = map('{:02x}'.format, bytes_addr)
    mac_addr = ':'.join(bytes_str).upper()
    return mac_addr
