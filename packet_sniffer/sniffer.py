import my_ethernet as eth
import my_ip as ip
import my_transport_layer as transport
import socket

def main():
    sniffing_socket = socket.socket(socket.AF_PACKET, socket.SOCK_RAW, socket.ntohs(3))

    while True:
        eth_proto, data = eth.print_ethernet(sniffing_socket)
        # ethernet protocol means IPv4
        if eth_proto == ip.PROTOCOLV4:
            proto, data = ip.print_ipv4(data)
            if proto == transport.ICMP_PROTO:
                transport.print_icmp(data)
            elif proto == transport.TCP_PROTO:
                transport.print_tcp(data)
            elif proto == transport.UDP_PROTO:
                transport.print_udp(data)
            else:
                transport.print_else(data)
        elif eth_proto == ip.PROTOCOLV6:
            proto, data = ip.print_ipv6(data)
            if proto == transport.ICMPV6_PROTO:
                transport.print_icmp(data)
            elif proto == transport.TCP_PROTO:
                transport.print_tcp(data)
            elif proto == transport.UDP_PROTO:
                transport.print_udp(data)
            else:
                transport.print_else(data)

main()
