This program is a basic sniffer of inernet traffic on your computer.

It enables you to see all ethernet frames, and all packets of IPv4 and IPv6. Also checks some more information for ICMP, ICMP6, TCP, UDP and SCTP protocols.

This sniffer is for terminal usage.

Usage on Linux:

-To start the program:

  sudo python3 sniffer.py

-To end program:

  ctr + c
