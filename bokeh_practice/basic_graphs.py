#importing bokeh
from bokeh.plotting import figure
from bokeh.io import output_file, show

#preparing data
x = [1,2,3,4,5]
y = [6,7,8,9,10]

#prepare output file
output_file("line-with-circles.html")

#create a figure
f = figure()

#create a plot
f.line(x,y)
f.circle(x,y)
#f.tringle(x,y)

#writing the plot in the figure
show(f)
