from bokeh.plotting import figure
from bokeh.io import output_file, show
import pandas

df = pandas.read_excel("verlegenhuken.xlsx")
temperature = list(df["Temperature"]/10)
pressure = list(df["Pressure"]/10)

output_file("temperature_and_pressure_graph.html")
f = figure(plot_width = 800, plot_height = 600, tools = 'pan')
f.title.text = "Temperature and Air Pressure"
f.title.text_font = "times"
f.title.text_color = "gray"
f.title.text_font_style = "bold"
f.xaxis.axis_label = "Temperature (deg C)"
f.yaxis.axis_label = "Pressure (hPa)"
f.circle(temperature, pressure, size=1.5)
show(f)
