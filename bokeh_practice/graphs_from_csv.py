from bokeh.plotting import figure
from bokeh.io import output_file, show
import pandas

df = pandas.read_csv("bachelors.csv")
engineers_per_year = list(df["Engineering"])
year = list(df["Year"])

output_file("engineers_per_year_grad.html")
f = figure(plot_width = 1000, plot_height = 800)
f.title.text = "Graduates in Engineering per year"
f.title.text_color = "Gray"
f.title.text_font = "times"
f.title.text_font_style = "bold"
f.xaxis.minor_tick_line_color = None
f.yaxis.minor_tick_line_color = None
f.xaxis.axis_label = "Year"
f.yaxis.axis_label = "Graduates"
f.line(year, engineers_per_year)
show(f)
