from bokeh.plotting import figure
from bokeh.io import output_file, show
import pandas

df = pandas.read_csv("adbe.csv", parse_dates=["Date"])
f = figure(width = 1200, height = 600, x_axis_type = "datetime")

f.line(df["Date"], df["Close"], color = "red", alpha = 0.7)
output_file("timeseries.html")
show(f)
