from itertools import combinations

def get_data():
    with open('input.txt','r') as file:
        content = file.readlines()
        return content

def part_1():
    data = get_data()
    for n in range(len(data)):
        data[n] = int(data[n])

    total = 0
    for n in range(len(data)):
        subtotal = 0
        for comb in combinations(data, n):
            if sum(comb) == 150:
                subtotal += 1
        total += subtotal
    return total

def part_2():
    data = get_data()
    for n in range(len(data)):
        data[n] = int(data[n])

    total = 0
    for n in range(len(data)):
        subtotal = 0
        for comb in combinations(data, n):
            if sum(comb) == 150:
                subtotal += 1
        total += subtotal
        if subtotal > 0:
            break
    return total

print(part_1())
print(part_2())
