password = "cqjxjnds"

def prop_new_pass(password):
    new_pass = list(password)
    if ord(new_pass[-1]) < 122:
        new_pass[-1] = chr(ord(new_pass[-1]) + 1)
    else:
        new_pass[-1] = 'a'
        for n in range(2,len(new_pass)):
            if ord(new_pass[-n]) < 122:
                new_pass[-n] = chr(ord(new_pass[-n]) + 1)
                break
            else:
                new_pass[-n] = 'a'

    password = ''
    for item in new_pass:
        password += item

    return password

def req1(password):
    if 'i' in password:
        return False
    elif 'o' in password:
        return False
    elif 'l' in password:
        return False
    else:
        return True

def req2(password):
    alphabeth = 'abcdefghijklmnopqrstuvwxyz'
    flag = False
    for n in range(len(alphabeth)-2):
        if alphabeth[n:n+3] in password:
            flag = True
            break
        else:
            continue
    return flag

def req3(password):
    alphabeth = 'abcdefghijklmnopqrstuvwxyz'
    check = 0
    for n in alphabeth:
        if (n+n) in password:
            check += 1
        else:
            continue
    if check >= 2:
        return True
    else:
        return False

def new_password(password):
    password = prop_new_pass(password)
    while True:
        if req1(password) and req2(password) and req3(password) == True:
            print(password)
            break
        else:
            password = prop_new_pass(password)
    return password

new_password(new_password(password))
