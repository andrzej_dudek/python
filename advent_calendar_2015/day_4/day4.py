import hashlib
from itertools import count

first = 'abcdef'
second = 'pqrstuv'
mine = 'ckczppom'

def find_md5_coin(init, num_of_zeros):
    for n in count(1):
        test = init + str(n)
        result = hashlib.md5(test.encode('utf-8')).hexdigest()
        if result[0:(1*num_of_zeros)] == '0'*num_of_zeros:
            print(result)
            print(n)
            break



find_md5_coin(first,5)
find_md5_coin(second,5)
find_md5_coin(mine,5)
find_md5_coin(mine,6)
