def get_instructions():
    with open('input.txt', 'r') as file:
        raw = file.readlines()
        instructions = list()
        for r in raw:
            instruction = r.replace(',' , '').replace(' ', '\n')
            instruction = instruction.splitlines()
            instructions.append(instruction)
        return instructions

registers = {'a': 0, 'b': 0}

def obey_instruction(instructions, n):
    if n >= len(instructions):
        return (0, 0)
    if n <= 0 :
        n = 0
    instruction = instructions[n]
    reg = instruction[1]

    if instruction[0] == 'hlf':
        registers[reg] /= 2
        n += 1
    elif instruction[0] == 'tpl':
        registers[reg] *= 3
        n += 1
    elif instruction[0] == 'inc':
        registers[reg] += 1
        n += 1
    elif instruction[0] == 'jmp':
        n += int(instruction[1])
    elif instruction[0] == 'jie':
        if registers[reg] % 2 == 0:
            n += int(instruction[2])
        else:
            n += 1
    elif instruction[0] == 'jio':
        if registers[reg] == 1:
            n += int(instruction[2])
        else:
            n += 1

    return (1, n)

def part_1():
    global registers
    registers = {'a': 0, 'b': 0}
    instructions = get_instructions()
    flag = 1
    n = 0
    while flag:
        flag, n = obey_instruction(instructions, n)

    return registers['b']

def part_2():
    global registers
    registers = {'a': 1, 'b': 0}
    instructions = get_instructions()
    flag = 1
    n = 0
    while flag:
        flag, n = obey_instruction(instructions, n)

    return registers['b']

print(part_1())
print(part_2())
