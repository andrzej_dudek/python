from itertools import combinations_with_replacement

def get_input():
    with open('input.txt', 'r') as file:
        content = file.readlines()
        return content

def part_1():
    data = get_input()
    ingredients = dict()
    for item in data:
        item = item.replace(' ', '\n').splitlines()
        key = item[0].rstrip(':')
        value = [int(item[2].rstrip(',')), int(item[4].rstrip(',')),
                 int(item[6].rstrip(',')), int(item[8].rstrip(',')),
                 int(item[10])]
        ingredients.update({key: value})

    total = list()

    for item in combinations_with_replacement(ingredients.keys(), 100):
        properties = [0,0,0,0]
        for word in item:
            properties[0] += ingredients[word][0]
            properties[1] += ingredients[word][1]
            properties[2] += ingredients[word][2]
            properties[3] += ingredients[word][3]
        for n in range(len(properties)):
            if properties[n] < 0:
                properties[n] = 0
        result = 1
        for item in properties:
            result *= item
        total.append(result)

    total.sort()
    return total[-1]

def part_2():
    data = get_input()
    ingredients = dict()
    for item in data:
        item = item.replace(' ', '\n').splitlines()
        key = item[0].rstrip(':')
        value = [int(item[2].rstrip(',')), int(item[4].rstrip(',')),
                 int(item[6].rstrip(',')), int(item[8].rstrip(',')),
                 int(item[10])]
        ingredients.update({key: value})

    total = list()

    for item in combinations_with_replacement(ingredients.keys(), 100):
        properties = [0,0,0,0]
        callories = 0
        for word in item:
            properties[0] += ingredients[word][0]
            properties[1] += ingredients[word][1]
            properties[2] += ingredients[word][2]
            properties[3] += ingredients[word][3]
            callories += ingredients[word][4]
        if callories == 500:
            for n in range(len(properties)):
                if properties[n] < 0:
                    properties[n] = 0
            result = 1
            for item in properties:
                result *= item
            total.append(result)
        else:
            continue
    total.sort()
    return total[-1]

print(part_1())
print(part_2())
