from itertools import permutations

def get_input():
    with open('input.txt', 'r') as file:
        content = file.readlines()
        return content

def part_1():
    guest_list = get_input()

    guests = set()
    total_points_list = []
    guest_dict = dict()

    for n in range(len(guest_list)):
        guest_list[n] = guest_list[n].replace(' ', '\n').splitlines()
        key =  guest_list[n][0] + guest_list[n][-1].rstrip('.')
        guests.add(guest_list[n][0])
        if guest_list[n][2] == 'gain':
            value = int(guest_list[n][3])
        else:
            value = -int(guest_list[n][3])
        guest_dict.update({key: value})


    for sitting in permutations(guests):
        total_points = 0
        for n in range(len(sitting)):
            if n == len(sitting) - 1:
                total_points += guest_dict[sitting[n]+sitting[n-1]]
                total_points += guest_dict[sitting[n]+sitting[0]]
            else:
                total_points += guest_dict[sitting[n]+sitting[n-1]]
                total_points += guest_dict[sitting[n]+sitting[n+1]]
        total_points_list.append(total_points)

    total_points_list.sort()
    return total_points_list[-1]

def part_2():
    guest_list = get_input()

    guests = set()
    total_points_list = []
    guest_dict = dict()

    for n in range(len(guest_list)):
        guest_list[n] = guest_list[n].replace(' ', '\n').splitlines()
        key =  guest_list[n][0] + guest_list[n][-1].rstrip('.')
        guests.add(guest_list[n][0])
        if guest_list[n][2] == 'gain':
            value = int(guest_list[n][3])
        else:
            value = -int(guest_list[n][3])
        guest_dict.update({key: value})

    for guest in guests:
        guest_dict.update({guest+'Me': 0})
        guest_dict.update({'Me'+guest: 0})

    guests.add('Me')

    for sitting in permutations(guests):
        total_points = 0
        for n in range(len(sitting)):
            if n == len(sitting) - 1:
                total_points += guest_dict[sitting[n]+sitting[n-1]]
                total_points += guest_dict[sitting[n]+sitting[0]]
            else:
                total_points += guest_dict[sitting[n]+sitting[n-1]]
                total_points += guest_dict[sitting[n]+sitting[n+1]]
        total_points_list.append(total_points)

    total_points_list.sort()
    return total_points_list[-1]

print(part_1())
print(part_2())
