import json

def get_input():
    data = json.load(open('input.json', 'r'))
    return data

def iteratete_through_list(object):
    for item in object:
        if type(item) == int or type(item) == float:
            global total_sum
            total_sum += item
        elif type(item) == list:
            iteratete_through_list(item)
        elif type(item) == dict:
            iteratete_through_list(item.keys())
            iteratete_through_list(item.values())


def iteratete_through_list_part2(object):
    for item in object:
        if type(item) == int or type(item) == float:
            global total_sum
            total_sum += item
        elif type(item) == list:
            iteratete_through_list_part2(item)
        elif type(item) == dict:
            #print('try_dict')
            if "red" in item.values():
                continue
            elif "red" in item.keys():
                continue
            else:
                #print('it_t_dict')
                iteratete_through_list_part2(item.keys())
                iteratete_through_list_part2(item.values())

global total_sum
total_sum = 0

iteratete_through_list(get_input())
print(total_sum)
total_sum = 0
print(total_sum)
iteratete_through_list_part2(get_input())
print(total_sum)
