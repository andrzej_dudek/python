dest_row = 2947
dest_col = 3029

code = 20151125
row = 1
col = 1

next = lambda x: ((x * 252533) % 33554393)

while True:
    if row == 1:
        row = col + 1
        col = 1
    else:
        row -= 1
        col += 1
    code = next(code)

    if row == dest_row and col == dest_col:
        break

print(code)
