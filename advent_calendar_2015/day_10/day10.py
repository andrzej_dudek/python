word = "1113122113"

def manipulate_word(word):
    sub_strings = []
    sub_string = ''
    for c in word:
        if len(sub_strings) == 0 and len(sub_string) == 0:
            sub_string = c
        else:
            if c == sub_string[-1]:
                sub_string += c
            else:
                sub_strings.append(sub_string)
                sub_string = c

    sub_strings.append(sub_string)

    #print(sub_strings)
    new_word = ''
    for item in sub_strings:
        new_word += str(len(item)) + item[0]

    #print(new_word)
    return new_word

for i in range(50):
    word = manipulate_word(word)

print(len(word))
