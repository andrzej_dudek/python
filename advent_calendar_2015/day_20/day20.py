num_of_presents = 36000000

presents_per_house = list()
house = 1

def find_dividers(x):
    dividers = list()
    for n in range(1,x+1):
        if x % n == 0:
            dividers.append(n)
    return dividers

#sum_presents = lambda x: sum((10*_x) for _x in x)

while True:
    dividers = find_dividers(house)
    presents = sum(dividers) * 10
    if presents >= num_of_presents:
        break
    else:
        house += 1
    if house % 5000 == 0:
        print(house)

print(house)
