from itertools import permutations

def get_input():
    with open('input.txt', 'r') as file:
        content = file.readlines()
        return content

routes_desc = get_input()
locations = set()
routes = {}
distances = []
for n in range(len(routes_desc)):
    fl, rest = routes_desc[n].split(' to ')
    sl, dist = rest.split(' = ')
    routes.update({fl+sl:int(dist),sl+fl:int(dist)})
    locations.add(fl)
    locations.add(sl)


for item in permutations(locations):
    distance = 0
    for r in range(len(item)):
        try:
            distance += routes[item[r]+item[r+1]]
        except:
            pass
    distances.append(distance)

distances.sort()
print(distances[0])
print(distances[-1])
