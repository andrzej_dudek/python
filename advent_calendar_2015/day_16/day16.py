def get_data():
    with open('input.txt','r') as file:
        content = file.readlines()
        return content

def part_1():
    data = get_data()
    keywords = ['children','cats','samoyeds','pomerians','akitas',
                'vizslas','goldfish','trees','cars','perfumes']
    formated_data = list()
    for line in data:
        line = line.replace(',','').replace(':','').replace(' ','\n').splitlines()
        line = line[2:]
        formated_data.append(line)

    aunts = list()
    for line in formated_data:
        aunt = []
        for key in keywords:
            if key in line:
                aunt.append(int(line[line.index(key) + 1]))
            else:
                aunt.append(None)
        aunts.append(aunt)

    original_aunt = [3,7,2,3,0,0,5,3,2,1]

    for n in range(len(aunts)):
        flags = [0,0,0,0,0,0,0,0,0,0]
        for k in range(len(original_aunt)):
            if aunts[n][k] == original_aunt[k] or aunts[n][k] == None:
                flags[k] = 1
        if sum(flags) == 10:
            return n+1

def part_2():
    data = get_data()
    keywords = ['children','cats','samoyeds','pomerians','akitas',
                'vizslas','goldfish','trees','cars','perfumes']
    formated_data = list()
    for line in data:
        line = line.replace(',','').replace(':','').replace(' ','\n').splitlines()
        line = line[2:]
        formated_data.append(line)

    aunts = list()
    for line in formated_data:
        aunt = []
        for key in keywords:
            if key in line:
                aunt.append(int(line[line.index(key) + 1]))
            else:
                aunt.append(None)
        aunts.append(aunt)

    original_aunt = [3,7,2,3,0,0,5,3,2,1]

    for n in range(len(aunts)):
        flags = [0,0,0,0,0,0,0,0,0,0]
        for k in range(len(original_aunt)):
            if k == 1 or k == 7:
                if aunts[n][k] == None or aunts[n][k] > original_aunt[k]:
                    flags[k] = 1
            elif k == 3 or k == 6:
                if aunts[n][k] == None or aunts[n][k] < original_aunt[k]:
                    flags[k] = 1
            else:
                if aunts[n][k] == original_aunt[k] or aunts[n][k] == None:
                    flags[k] = 1
        if sum(flags) == 10:
            return n+1

print(part_1())
print(part_2())
