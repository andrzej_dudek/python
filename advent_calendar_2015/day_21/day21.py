from itertools import combinations

boss_ = {'hp':104, 'damage': 8, 'armor': 1}
player_ = {'hp': 100, 'damage': 0, 'armor': 0}
weapons = [[8,4,0], [10,5,0], [25,6,0], [40,7,0], [74,8,0]]
armors = [[13,0,1], [31,0,2], [53,0,3], [75,0,4], [102,0,5]]
rings = [[25,1,0], [50,2,0], [100,3,0], [20,0,1], [40,0,2], [80,0,3]]

def fight(player, boss):
    player_damage = player[1] - boss[2]
    if player_damage <= 0:
        player_damage = 1
    boss_damage = boss[1] - player[2]
    if boss_damage <= 0:
        boss_damage = 1
    #print(player_damage)
    while True:
        boss[0] -= player_damage
        if boss[0] <= 0:
            win_lose = True
            #print('win')
            break
        player[0] -= boss_damage
        if player[0] <= 0:
            win_lose = False
            #print('lose')
            break
    return win_lose


def part_1(boss_, player_, weapons, armors, rings):
    cost = list()
    for weapon in weapons:
        for n in range(len(armors) + 1):
            if n == len(armors):
                armor = [0,0,0]
            else:
                armor = armors[n]
            for i in range(3):
                for comb in combinations(rings,i):
                    if len(comb) == 0:
                        #player = player_
                        #boss = boss_
                        player = [player_['hp'], player_['damage'], player_['armor']]
                        boss = [boss_['hp'], boss_['damage'], boss_['armor']]
                        current_cost = 0
                        current_cost += weapon[0] + armor[0]
                        player[1] += weapon[1]
                        player[2] += armor[2]
                        if fight(player, boss):
                            cost.append(current_cost)
                    else:
                        player = [player_['hp'], player_['damage'], player_['armor']]
                        boss = [boss_['hp'], boss_['damage'], boss_['armor']]
                        current_cost = 0
                        current_cost += weapon[0] + armor[0]
                        player[1] += weapon[1]
                        player[2] += armor[2]
                        for item in comb:
                            current_cost += item[0]
                            player[1] += item[1]
                            player[2] += item[2]
                            if fight(player, boss):
                                cost.append(current_cost)


    cost.sort()
    #print(cost)
    return cost[0]

def part_2(boss_, player_, weapons, armors, rings):
    cost = list()
    for weapon in weapons:
        for n in range(len(armors) + 1):
            if n == len(armors):
                armor = [0,0,0]
            else:
                armor = armors[n]
            for i in range(3):
                for comb in combinations(rings,i):
                    if len(comb) == 0:
                        #player = player_
                        #boss = boss_
                        player = [player_['hp'], player_['damage'], player_['armor']]
                        boss = [boss_['hp'], boss_['damage'], boss_['armor']]
                        current_cost = 0
                        current_cost += weapon[0] + armor[0]
                        player[1] += weapon[1]
                        player[2] += armor[2]
                        if not fight(player, boss):
                            #print(current_cost)
                            cost.append(current_cost)
                    else:
                        player = [player_['hp'], player_['damage'], player_['armor']]
                        boss = [boss_['hp'], boss_['damage'], boss_['armor']]
                        current_cost = 0
                        current_cost += weapon[0] + armor[0]
                        player[1] += weapon[1]
                        player[2] += armor[2]
                        for item in comb:
                            current_cost += item[0]
                            player[1] += item[1]
                            player[2] += item[2]
                            if not fight(player, boss):
                                #print(current_cost)
                                cost.append(current_cost)


    cost.sort()
    #print(cost)
    return cost[-1]


print(part_1(boss_, player_, weapons, armors, rings))
print(part_2(boss_, player_, weapons, armors, rings))
