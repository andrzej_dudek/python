from itertools import combinations
from functools import reduce
from operator import mul

def get_data():
    with open('input.txt','r') as file:
        lines = file.readlines()
        lines = [int(line) for line in lines]
        return lines

def problem(groups):
    data = get_data()
    group_size = sum(data) / groups
    for i in range(len(data)):
        quantum_entanglements = [reduce(mul, c) for c in combinations(data, i)
               if sum(c) == group_size]
        if quantum_entanglements:
            return min(quantum_entanglements)

def part_1():
    return problem(3)

def part_2():
    return problem(4)

print(part_1())
print(part_2())
