from random import shuffle

def get_data():
    with open('input.txt') as file:
        content = file.readlines()
        molecule  = content[-1].strip()
        replacement_dict = dict()
        for n in range(len(content) - 1):
            content[n] = content[n].strip()
            if len(content[n]):
                key, value = content[n].split(' => ')
                if key not in replacement_dict.keys():
                    replacement_dict.update({key:[value]})
                else:
                    replacement_dict[key].append(value)
        return molecule, replacement_dict

def get_data_2():
    with open('input.txt') as file:
        content = file.readlines()
        molecule  = content[-1].strip()
        replacement = list()
        for n in range(len(content) - 1):
            content[n] = content[n].strip()
            if len(content[n]):
                value, key = content[n].split(' => ')
                replacement.append((key,value))
        return molecule, replacement

def part_1():
    molecule, replacement_dict = get_data()

    possibilities = set()

    for item in replacement_dict.keys():
        num = molecule.count(item)
        start = 0
        for n in range(num):
            start = molecule.find(item,start+1)
            for option in replacement_dict[item]:
                new = molecule[0:start] + option + molecule[start + len(item):]
                possibilities = possibilities | {new}

    return len(possibilities)

def part_2():
    molecule, replacement = get_data_2()
    end = 'e'
    count = 0
    current = molecule
    while current != end:
        temporary = current
        for item, rep in replacement:
            if item in current:
                current = current.replace(item, rep, 1)
                count += 1
        if temporary == current:
            count = 0
            current = molecule
            shuffle(replacement)
    return count

print(part_1())
print(part_2())
