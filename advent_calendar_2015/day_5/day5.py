def get_input():
    with open('input.txt', 'r') as file:
        content = file.readlines()
        return content

def count_nice_words(words):
    nice_words = 0
    for word in words:
        if validate(word) == True:
            nice_words += 1
        else:
            continue
    return nice_words

def validate(word):
    if cond1(word) and cond2(word) and cond3(word) == True:
        return True
    else:
        return False

def cond1(word):
    strings = ['ab', 'cd', 'pq', 'xy']
    if any(string in word for string in strings):
        return False
    else:
        return True

def cond2(word):
    vowels = ['a', 'e', 'i', 'o', 'u']
    count = 0
    for ch in word:
        if any(ch == vowel for vowel in vowels):
            count += 1
        else:
            pass
    if count >= 3:
        return True
    else:
        return False


def cond3(word):
    alphabeth = 'abcdefghijklmnopqrstuvwxyz'
    for letter in alphabeth:
        if letter*2 in word:
            return True
        else:
            continue
    return False

def count_nice_words_part_2(words):
        nice_words = 0
        for word in words:
            if validate_part_2(word) == True:
                nice_words += 1
            else:
                continue
        return nice_words

def validate_part_2(word):
    if cond4(word) and cond5(word) == True:
        return True
    else:
        return False

def cond4(word):
    alphabeth = 'abcdefghijklmnopqrstuvwxyz'
    for first_letter in alphabeth:
        for second_letter in alphabeth:
            if first_letter + second_letter + first_letter in word:
                return True
    return False

def cond5(word):
    alphabeth = 'abcdefghijklmnopqrstuvwxyz'
    for first_letter in alphabeth:
        for second_letter in alphabeth:
            if word.count(first_letter+second_letter) >= 2:
                return True
    return False



print(count_nice_words(get_input()))
print(count_nice_words_part_2(get_input()))
