import numpy

def get_input():
    with open('input.txt', 'r') as file:
        content = file.readlines()
        return content

def create_lights_array():
    lights = numpy.zeros((1000,1000), dtype=int)
    return lights

def follow_the_instructions():
    instructions = get_input()
    lights = create_lights_array()
    for instruction in instructions:
        lights = obey_instrucion(instruction, lights)
    lights_litt = lights.sum()
    return lights_litt

def obey_instrucion(instruction, array):
    if 'turn on' in instruction:
        array = turn_on(instruction, array)
    elif 'turn off' in instruction:
        array = turn_off(instruction, array)
    elif 'toggle' in instruction:
        array = toggle(instruction, array)
    return array

def extract_coordinates(instruction, word):
    instruction = instruction.lstrip(word)
    instruction = instruction.replace(' ', '\n').splitlines()
    xs, ys = instruction[0].split(',')
    xe, ye = instruction[2].split(',')
    xs = int(xs)
    ys = int(ys)
    xe = int(xe)
    ye = int(ye)
    return (xs,ys,xe,ye)


def turn_on(instruction, array):
    xs, ys, xe, ye = extract_coordinates(instruction, 'turn on ')
    array[xs:xe+1,ys:ye+1] = 1
    return array

def turn_off(instruction, array):
    xs, ys, xe, ye = extract_coordinates(instruction, 'turn off ')
    array[xs:xe+1,ys:ye+1] = 0
    return array

def toggle(instruction, array):
    xs, ys, xe, ye = extract_coordinates(instruction, 'toggle ')
    for k in range(xs,xe+1):
        for l in range(ys, ye+1):
            if array[k,l] == 0:
                array[k,l] = 1
            elif array[k,l] == 1:
                array[k,l] = 0
    return array

def follow_the_instructions_2():
    instructions = get_input()
    lights = create_lights_array()
    for instruction in instructions:
        lights = obey_instrucion_2(instruction, lights)
    total_brightnes = lights.sum()
    return total_brightnes

def obey_instrucion_2(instruction, array):
    if 'turn on' in instruction:
        array = turn_on_2(instruction, array)
    elif 'turn off' in instruction:
        array = turn_off_2(instruction, array)
    elif 'toggle' in instruction:
        array = toggle_2(instruction, array)
    return array

def turn_on_2(instruction, array):
    xs, ys, xe, ye = extract_coordinates(instruction, 'turn on ')
    array[xs:xe+1,ys:ye+1] += 1
    return array

def turn_off_2(instruction, array):
    xs, ys, xe, ye = extract_coordinates(instruction, 'turn off ')
    for k in range(xs, xe+1):
        for l in range(ys, ye+1):
            if array[k,l] > 0:
                array[k,l] -= 1
            else:
                continue
    return array

def toggle_2(instruction, array):
    xs, ys, xe, ye = extract_coordinates(instruction, 'toggle ')
    array[xs:xe+1,ys:ye+1] += 2
    return array

print(follow_the_instructions())
print(follow_the_instructions_2())
