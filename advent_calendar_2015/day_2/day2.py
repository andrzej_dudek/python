def get_input():
    with open('input.txt', 'r') as file:
        content = file.readlines()
        return content

def get_total_area(dim_list):
    total_area = 0
    for item in dim_list:
        item = item.replace('x', '\n').splitlines()
        w, l, h = int(item[0]), int(item[1]), int(item[2])
        wl = w * l
        wh = w * h
        lh = l * h
        total_area += 2*wl + 2*wh + 2*lh
        if wl <= wh:
            smallest = wl
        else:
            smallest= wh
        if smallest > lh:
            smallest = lh
        total_area += smallest
    return total_area

def sort_and_return_2(sorting_list):
    sorting_list.sort()
    return sorting_list[0:2]


def get_ribbon_length(dim_list):
    total_length = 0
    for item in dim_list:
        item = item.replace('x', '\n').splitlines()
        w, l, h = int(item[0]), int(item[1]), int(item[2])
        total_length += w * l * h
        smallest_dim = sort_and_return_2([w,l,h])
        total_length += 2*smallest_dim[0] + 2*smallest_dim[1]
    return total_length


print(get_total_area(get_input()))
print(get_ribbon_length(get_input()))
