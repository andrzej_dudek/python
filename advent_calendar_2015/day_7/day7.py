def get_input():
    with open('input.txt', 'r') as file:
        content = file.readlines()
        return content

def circuit_part_1_and_2(inject=False, b_state=0):
    vars = {}
    two_el_inst = []
    inst_to_del = []

    instructions = get_input()
    for n in range(len(instructions)):
        command, dest = instructions[n].split('->')
        try:
            vars.update({dest.strip() : int(command)})
            inst_to_del.append(instructions[n])
        except:
            vars.update({dest.strip(): None})
            if any(operator in instructions[n] for operator in ['AND','OR','RSHIFT','LSHIFT']):
                two_el_inst.append(instructions[n])
            else:
                if 'NOT' in instructions[n]:
                    instructions[n] =  [command[3:].strip(), dest.strip(), 'NOT']
                else:
                    instructions[n] = [command.strip(), dest.strip()]
        #print(command)
        #print(dest)

    #print(len(vars))

    for item in two_el_inst:
        instructions.remove(item)
    for item in inst_to_del:
        instructions.remove(item)

    #print(instructions)
    #print(type(vars['b']))
    if inject:
        vars['b'] = b_state

    for n in range(len(two_el_inst)):
        command, dest = two_el_inst[n].split('->')
        if 'AND' in command:
            el1, el2 = command.split('AND')
            try:
                el1 = int(el1)
            except:
                el1 = el1.strip()
            two_el_inst[n] = [el1,el2.strip(),dest.strip(),'&']
        elif 'OR' in command:
            el1, el2 = command.split('OR')
            try:
                el1.strip()
                el1 = int(el1)
            except:
                el1 = el1.strip()
            two_el_inst[n] = [el1,el2.strip(),dest.strip(),'|']
        elif 'RSHIFT' in command:
            el1, el2 = command.split('RSHIFT')
            two_el_inst[n] = [el1.strip(),int(el2),dest.strip(),'>>']
        elif 'LSHIFT' in command:
            el1, el2 = command.split('LSHIFT')
            two_el_inst[n] = [el1.strip(),int(el2),dest.strip(),'<<']

    flag = True

    while flag:

        flag = False

        for item in instructions:
            if vars[item[0]] != None:
                if len(item) == 2:
                    vars[item[1]] = vars[item[0]]
                elif len(item) == 3:
                    vars[item[1]] = ~vars[item[0]]
                instructions.remove(item)
            else:
                continue

        for item in two_el_inst:
            #print(item[0], item[1], item[2])
            #print(type(item[0]), type(item[1]))
            if type(item[0]) == int:
                if vars[item[1]] != None:
                    if item[3] == '&':
                        vars[item[2]] = item[0] & vars[item[1]]
                    elif item[3] == '|':
                        vars[item[2]] = item[0] | vars[item[1]]
                else:
                    continue
            elif type(item[1]) == int:
                if vars[item[0]] != None:
                    if item[3] == '>>':
                        vars[item[2]] = vars[item[0]] >> item[1]
                    elif item[3] == '<<':
                        vars[item[2]] = vars[item[0]] << item[1]
                else:
                    continue
            elif vars[item[0]] != None and vars[item[1]] != None:
                if item[3] == '&':
                    vars[item[2]] = vars[item[0]] & vars[item[1]]
                elif item[3] == '|':
                    vars[item[2]] = vars[item[0]] | vars[item[1]]
            else:
                continue
            two_el_inst.remove(item)

        if any(x == None for x in vars.values()):
            flag = True

    return vars['a']

print(circuit_part_1_and_2())
print(circuit_part_1_and_2(True,circuit_part_1_and_2()))
