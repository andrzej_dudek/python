def what_floor(input):
    # input is a series of '(' which means 1 floor up ')'
    # which means 1 floor down, we are starting from floor 0 and
    # we need to calculate the final floor

    floor = 0
    moves = {'(': 0, ')': 0}
    for sign in input:
        if sign == '(':
            moves[sign] += 1
        elif sign == ')':
            moves[sign] += 1
        else:
            continue
    floor = floor + moves['('] - moves[')']
    return floor

def get_input():
    with open('input.txt', 'r') as file:
        content = file.read()
        return content

def when_enter_basement(input):
    floor = 0
    position = 0
    for sign in input:
        if sign == '(':
            floor += 1
        elif sign == ')':
            floor -= 1

        position += 1

        if floor < 0:
            break

    return position

print(what_floor(get_input()))
print(when_enter_basement(')'))
print(when_enter_basement('()())'))
print(when_enter_basement(get_input()))
