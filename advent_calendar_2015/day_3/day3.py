import numpy

def get_input():
    with open('input.txt', 'r') as file:
        content = file.read()
        return content

def create_a_map():
    map = numpy.zeros((2000,2000))
    return map

def map_houses(instructions):
    map = create_a_map()
    rows, cols = map.shape
    location = {'x':int(rows/2), 'y':int(cols/2)}
    map[location['x'],location['y']] = 1
    for instruction in instructions:
        location = movement(instruction, location)
        map[location['x'],location['y']] = 1

    number_of_houses = map.sum()
    return number_of_houses

def movement(instruction, location):
    if instruction == '^':
        location['y'] -= 1
    elif instruction == '<':
        location['x'] -= 1
    elif instruction == 'v':
        location['y'] += 1
    elif instruction == '>':
        location['x'] += 1
    else:
        pass
    return location


def map_houses_next_year(instructions):
    map = create_a_map()
    rows, cols = map.shape
    santa_loc = {'x':int(rows/2), 'y':int(cols/2)}
    robo_loc = {'x':int(rows/2), 'y':int(cols/2)}
    map[santa_loc['x'], santa_loc['y']] = 1
    santa_index = [x for x in range(len(instructions)) if x%2 == 0]
    for n in santa_index:
        santa_loc = movement(instructions[n], santa_loc)
        try:
            robo_loc = movement(instructions[n+1], robo_loc)
        except:
            pass
        map[santa_loc['x'], santa_loc['y']] = 1
        map[robo_loc['x'], robo_loc['y']] = 1

    return map.sum()


print(map_houses(get_input()))
print(map_houses_next_year(get_input()))
