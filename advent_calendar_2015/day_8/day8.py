def get_input():
    with open('input.txt','r') as file:
        content = file.readlines()
        return content

total_space_part_1 = 0
total_space_part_2 = 0
words = get_input()
for item in words:
    total_space_part_1 += (len(item) - len(eval(item)) - 1)
    total_space_part_2 += 2 + item.count('\\') + item.count('"')

print(total_space_part_1)
print(total_space_part_2)
