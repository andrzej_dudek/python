def get_input():
    with open('input.txt') as file:
        content = file.readlines()
        return content

def calcualte_distance(arr, time):
    full_cycles = int(time / (arr[1] + arr[2]))
    rest = time - full_cycles*(arr[1] + arr[2])
    distance = full_cycles * (arr[0] * arr[1])
    if rest < arr[1]:
        distance += rest * arr[0]
    else:
        distance += arr[0] * arr[1]
    return distance

def part_1():
    all_data = get_input()
    data = dict()

    for n in range(len(all_data)):
        all_data[n] = all_data[n].replace(' ', '\n').splitlines()
        key = all_data[n][0]
        value = [int(all_data[n][3]), int(all_data[n][6]), int(all_data[n][13])]
        data.update({key: value})

    distances = list()

    for item in data:
        distances.append(calcualte_distance(data[item], 2503))

    distances.sort()
    return distances[-1]

def calc_curr_distance(distances, key, arr, second):
    if second % (arr[1] + arr[2]) < arr[1]:
        distances[key] += arr[0]
    else:
        distances[key] += 0
        pass

def update_scoretable(scoretable, distances):
    distance = list(distances.values())
    distance.sort()
    distance = distance[-1]
    for item in scoretable.keys():
        if distances[item] == distance:
            scoretable[item] += 1
        else:
            continue
    return scoretable

def part_2():
    all_data = get_input()
    data = dict()
    scoretable = dict()
    current_distance = dict()

    for n in range(len(all_data)):
        all_data[n] = all_data[n].replace(' ', '\n').splitlines()
        key = all_data[n][0]
        value = [int(all_data[n][3]), int(all_data[n][6]), int(all_data[n][13])]
        data.update({key: value})
        scoretable.update({key: 0})
        current_distance.update({key: 0})

    for n in range(0,2503+1):
        for key in current_distance.keys():
            calc_curr_distance(current_distance, key, data[key], n)
        scoretable = update_scoretable(scoretable, current_distance)

    top_value = sorted(scoretable.values())[-1]
    for item in scoretable:
        if scoretable[item] == top_value:
            return (item, top_value)

print(part_1())
print(part_2())
